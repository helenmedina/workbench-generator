# README #

Workbench prototype.

### What is this repository for? ###

##### Quick summary

	This generator generates a workbench with the following functions:

	- Automate repetitive user actions. 

	- Generate algorithms structure define in file configuration.

![alt text](images/workbench.png)


### How do I get set up? ###

##### Summary of set up

	Open the project with Qt Creator

##### Configuration

	Configure the algorithms and the parameters in the config file

##### Dependencies

	-Ruby

	-QTCreator

	-OpenCV

##### How to run tests

	Run the prototype with Qt Creator
