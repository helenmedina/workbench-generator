class Detector

	SChildren = Struct.new(:name)
	SVirtualInputs = Struct.new(:name, :type)

	attr_accessor :name

	def initialize(name)

		@nameDetector = name
		@arrayChildren = []
		@arrayVirtualInput = []

	end
	#add the class name that inherits from the interface detector
	def insertChildren(child)

		@child = SChildren.new(child[:name]) 
		@arrayChildren.push(@child)

	end

	#insert the input parameters of the virtual method of the interface detector class 
	def insertVirtualInput(inputV)

		@input = SVirtualInputs.new(inputV[:name], inputV[:type]) 
		@arrayVirtualInput.push(@input)

	end
	
	#return classes name that inherits from interface class
	def getChildren

	 return @arrayChildren

	end

	# return input parameters
	def getVirtualInput

	 return @arrayVirtualInput

	end

	#type of the return of the virtual 
	def setOutputMethod(output)

	 @output = output

	end

	#name of the virtual method to implement
	def setNameMethod(name)

	 @name = name

	end

	def getNameMethod

	 	return @name

	end

	def getOutputMethod

	 	return @output

	end

	def getName
		return @nameDetector

	end

	
end



