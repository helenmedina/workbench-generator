#include "player.h"
#include <iostream>
#include "shared/utilities.h"
#include "QMessageBox"

PlayerWidget::PlayerWidget(QWidget *parent) : QWidget(parent)
{
    m_bStop = true;
}

PlayerWidget::~PlayerWidget()
{
    if (capture.isOpened())
        capture.release();

}

bool PlayerWidget::loadVideo(QString filename)
{

    bool isOpened = false;
    capture.open(filename.toStdString());

    if( capture.isOpened() )
    {
        isOpened = true;
        int frameRate = (int) capture.get(CV_CAP_PROP_FPS);
        setDelay(NUM_FRAMES/frameRate);

        updateImage();

    }
    else
        isOpened = false;


    return isOpened;
}

void PlayerWidget::play()
{

    m_bStop = false;

    while(!m_bStop)
    {
        updateImage();
    }
}

void PlayerWidget::updateImage()
{
    cv::Mat frame;

    if (!capture.read(frame))
    {
        m_bStop = true;
    }
    else
    {
        updateImage(frame);
    }

}

void PlayerWidget::updateImage(cv::Mat& newFrame)
{

    getImage(newFrame, image);
    setFrame(newFrame);
    emit processImage(image);
}

void PlayerWidget::setFrame(cv::Mat& frame)
{

    currentFrame = frame;
}
cv::Mat& PlayerWidget::getFrame()
{

    return currentFrame ;
}

void PlayerWidget::getImage(cv::Mat &frame, QImage &image)
{
    cv::Mat imageInput;
    cv::cvtColor(frame, imageInput, CV_BGR2RGB);

    if (imageInput.channels()== 3)
        image = QImage ((const unsigned char*)(imageInput.data), imageInput.cols,imageInput.rows,QImage::Format_RGB888).copy();
    else if (imageInput.channels()== 1)
        image = QImage((const unsigned char*)(imageInput.data), imageInput.cols,imageInput.rows,QImage::Format_Indexed8).copy();
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Video format not recognized.");
        msgBox.exec();
    }

    frame.release();
    frame= NULL;
    imageInput.release();
    imageInput = NULL;
}


long long int PlayerWidget::getNumberOfFrames()
{
    return capture.get(CV_CAP_PROP_FRAME_COUNT);
}

long long int PlayerWidget::getCurrentFrame()
{
    return capture.get(CV_CAP_PROP_POS_FRAMES);
}

void PlayerWidget::setCurrentFrame(long long int iFrameNumber)
{
    currentF = iFrameNumber;
    capture.set(CV_CAP_PROP_POS_FRAMES, iFrameNumber);
}

void PlayerWidget::release()
{
    capture.release();
}

void PlayerWidget::setDelay(int iDelay)
{
    m_delay = iDelay;
}

int PlayerWidget::getDelay()
{
    return m_delay;
}

void PlayerWidget::stop()
{
    m_bStop = true;
}

cv::VideoCapture& PlayerWidget::getVideoCapture()
{
    return capture;
}
