#include "ellipseitem.h"
#include <QBrush>
#include <QMenu>
#include <QAction>
#include <QGraphicsSceneContextMenuEvent>

EllipseItem::EllipseItem(float x, float y, float w, float h) : QGraphicsEllipseItem(x, y, w, h)
{
    this->setBrush(QBrush(Qt::red));
}

void EllipseItem::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    QMenu menu;

    QAction *removeAction = menu.addAction("Remove");

    QAction *selectedAction = menu.exec(event->screenPos());

    if (selectedAction)
    {
        if ( selectedAction->text() == removeAction->text())
            emit removeMarkerFromScene(event->scenePos());
    }

}

void EllipseItem::setText(QString text)
{
    ellipseLabel = text;
}

QString EllipseItem::getText()
{
    return ellipseLabel;
}
