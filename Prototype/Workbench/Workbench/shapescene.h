#ifndef SHAPESCENE_H
#define SHAPESCENE_H
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsView>
#include <QKeyEvent>
#include "shared/shared.h"

class QLineEdit;
class RectangleItem;
class QInputDialog;

class ShapeScene : public QGraphicsScene
{
    Q_OBJECT
public:
    enum Mode {NoMode, DrawRectangle = 1, SelectionMode = 2};
    ShapeScene(QObject* parent = 0);
    void setMode(Mode mode);
    const std::vector<RectangleItem*>& getSelectedItems();
    void addNewRectangleItem(RectangleItem* newItem);
    void removeRectItem(RectangleItem* newItem, int);
    void addNewItemFromController(objectDetails);
    void removeAllItems();
    int getPositionX();
    int getPositionY();
    ~ShapeScene();

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
    void reserveMaxRectItems(int);
    void setUpConnectionsPerRectItem(RectangleItem* rectItem);
    void makeItemsControllable(bool areControllable);
    RectangleItem* getNextRectangleItem();
    RectangleItem* getCurrentRectangleItem();
    void drawRectangleItem(int x, int y, int width, int height);
    bool hasRectangleItemsReachlimit();
    void addNewItemToScene(RectangleItem* newItem);
    void setInitParametersForRectangleItem(RectangleItem* item, int x, int y, sColor color);
    void removeMyItem(QGraphicsItem *item);
    void removeText(QString text);

private:

    Mode sceneMode;
    QPointF origPoint;
    bool pressed;
    QLineEdit* pLineEdit;
    std::vector<RectangleItem*>rectangleUI;
    RectangleItem **rectangle;
    objectDetails itemModified;
    const int MAXIMUM_PLAYERS = 50;
    int currentRectangle;
    bool bIsNewItem;
    bool isItemModified;
    int positionX;
    int positionY;


public slots:
    void removeMyItem(const QPointF & point);
    void addLabel(const QPointF & point);
    void addName(const QPointF & point);
    void changeAllColor(QColor);
    void changeColor(QColor, const QPointF & point);
    void modifyObject(const QPointF &);

signals:
    void emitName(QString);

};

#endif // SHAPESCENE_H
