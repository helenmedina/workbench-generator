#ifndef LINESELECTOR_H
#define LINESELECTOR_H
#include <QDialog>
#include <QRect>
#include "shared/shared.h"

class QRadioButton;
class QGroupBox;
class QGraphicsView;
class QGraphicsScene;
class QVBoxLayout;
class QLineEdit;
class QHBoxLayout;



class initializerSelector : public QDialog
{
    Q_OBJECT

public:
    initializerSelector(QWidget *parent = 0);
    sPoints getPointSelected();


private:

    void setUpConfiguration();
    void setUpSoccerView();
    void setUpMarkersSelectionSide1A(QVBoxLayout*);
    void setUpMarkersSelectionSide2A(QVBoxLayout*);
    void setUpMarkersSelectionSide1B(QVBoxLayout*);
    void setUpMarkersSelectionSide2B(QVBoxLayout*);
    void configureLineEdit(QLineEdit*);
    void getPointValue(const QString &nameItem, sPoints &point);
    void checkItemWhenPointIsSelected(QLayout *layout);
    bool getPointFromItemSelected(QVBoxLayout* layoutBox, sPoints &point);
    void resetCheckboxItem(QVBoxLayout* layoutBox);


    //configure coordinates x, y
    //Side A
    void addPoint1A(QHBoxLayout *);
    void addPoint2A(QHBoxLayout *);
    void addPoint3A(QHBoxLayout *);
    void addPoint4A(QHBoxLayout *);
    void addPoint5A(QHBoxLayout *);
    void addPoint6A(QHBoxLayout *);
    void addPoint7A(QHBoxLayout *);
    void addPoint8A(QHBoxLayout *);
    void addPoint9A(QHBoxLayout *);
    void addPoint10A(QHBoxLayout *);
    void addPoint11A(QHBoxLayout *);
    void addPoint12A(QHBoxLayout *);
    void addPoint13A(QHBoxLayout *);
    void addPoint14A(QHBoxLayout *);
    void addPoint15A(QHBoxLayout *);

    //Side B
    void addPoint1B(QHBoxLayout *);
    void addPoint2B(QHBoxLayout *);
    void addPoint3B(QHBoxLayout *);
    void addPoint4B(QHBoxLayout *);
    void addPoint5B(QHBoxLayout *);
    void addPoint6B(QHBoxLayout *);
    void addPoint7B(QHBoxLayout *);
    void addPoint8B(QHBoxLayout *);
    void addPoint9B(QHBoxLayout *);
    void addPoint10B(QHBoxLayout *);
    void addPoint11B(QHBoxLayout *);
    void addPoint12B(QHBoxLayout *);
    void addPoint13B(QHBoxLayout *);
    void addPoint14B(QHBoxLayout *);
    void addPoint15B(QHBoxLayout *);


signals:
    void checkPointsSelectorRule();

public slots:
    void closeDialog();
    void resetSelectionMarkers();

private:
    QGraphicsView *view;
    QGraphicsScene *scene;
    QGroupBox *groupBoxSideA;
    //QGroupBox *groupBoxSideB;

    QPushButton *OKButtom;
    QRect geometry;
    QHBoxLayout *layoutView;
    QHBoxLayout *layoutButton;
    QVBoxLayout *verticalLayoutForGroupBoxSide1A;
    QVBoxLayout *verticalLayoutForGroupBoxSide2A;
    QVBoxLayout *verticalLayoutForGroupBoxSide1B;
    QVBoxLayout *verticalLayoutForGroupBoxSide2B;
    QHBoxLayout *layoutForGroupBoxes;
    PointsField* points;
    int width;
    int height;

};

#endif // LINESELECTOR_H
