#include <selector/initializerselector.h>
#include <QRadioButton>
#include <QGroupBox>
#include <QCheckBox>
#include <QLineEdit>

InitializerSelector::InitializerSelector(QWidget *parent) : QDialog(parent)
{
    layoutForGroupBoxes = new QVBoxLayout;
    detectByColorGroup = new QVBoxLayout;

    OKButtom = new QPushButton("Ok");
    //Creates the boxes (categories). These boxes should contain the algorithms that belongs in this category

    setUpSelection(detectByColorGroup);//add the elements for the color category
    groupBoxColor = new QGroupBox( "Color");
    QGroupBox *groupBoxThe = new QGroupBox( "Threshold");
    QVBoxLayout *detectByThGroup = new QVBoxLayout;
    groupBoxColor->setLayout(detectByColorGroup); //applies an order between elements same category

    setUpSelection2(detectByThGroup);//add the elements for the threshold category
    groupBoxThe->setLayout(detectByThGroup);

    layoutForGroupBoxes->addWidget(groupBoxColor);
    layoutForGroupBoxes->addWidget(groupBoxThe);
    QVBoxLayout *mainLayout = new QVBoxLayout;

    mainLayout->addLayout(layoutForGroupBoxes);//Add ll elements to the main layout
    this->setLayout(mainLayout);
}

void InitializerSelector::setUpSelection(QVBoxLayout *verticalLayout)
{
    verticalLayout->setSizeConstraint(QLayout::SetFixedSize);

//Add all algorithms to the category color
	QHBoxLayout *layoutDominantcolorH = new QHBoxLayout;
    addSelectorByDominantcolor(layoutDominantcolorH);

	QHBoxLayout *layoutKmeanscolorH = new QHBoxLayout;
    addSelectorByKmeanscolor(layoutKmeanscolorH);

	QHBoxLayout *layoutHistogramcolorH = new QHBoxLayout;
    addSelectorByHistogramcolor(layoutHistogramcolorH);

    // sets the alyout
    verticalLayout->addLayout(layoutDominantcolorH);


    verticalLayout->addLayout(layoutKmeanscolorH);

    verticalLayout->addLayout(layoutHistogramcolorH);

}
void InitializerSelector::setUpSelection2(QVBoxLayout *verticalLayout)
{
    // Add all algorithms to the category threshold
    verticalLayout->setSizeConstraint(QLayout::SetFixedSize);

    QHBoxLayout *layoutDominantcolorH = new QHBoxLayout;
    addSelectorByGrayThreshold(layoutDominantcolorH);

    verticalLayout->addLayout(layoutDominantcolorH);

}

void InitializerSelector::addSelectorByDominantcolor(QHBoxLayout *layout)
{
    //Add radio button widget
    QRadioButton *radioColorSelectorByDominantcolor = new QRadioButton(tr("Detector based on Dominantcolor"));
    radioColorSelectorByDominantcolor->setVisible(true);
     //Add checkbox widget
    QCheckBox *checkboxDominantcolorSelector = new QCheckBox(this);
    checkboxDominantcolorSelector->setEnabled(false);
     //Add order of teh algorithm widget
    QLineEdit *orderOfDominantcolor = new QLineEdit ("1");
    //connect signal between widgets
    QObject::connect(radioColorSelectorByDominantcolor, SIGNAL(clicked(bool)), checkboxDominantcolorSelector, SLOT(setEnabled(bool)));

    //add widgets to the layout
    layout->addWidget(radioColorSelectorByDominantcolor);
    layout->addWidget(checkboxDominantcolorSelector);
    layout->addWidget(orderOfDominantcolor);
}

void InitializerSelector::addSelectorByGrayThreshold(QHBoxLayout *layout)
{
    QRadioButton *radioColorSelectorByDominantcolor2 = new QRadioButton(tr("Detector based on GrayThreshold"));
    radioColorSelectorByDominantcolor2->setVisible(true);
    QCheckBox *checkboxDominantcolorSelector2 = new QCheckBox(this);
    checkboxDominantcolorSelector2->setEnabled(false);
    QLineEdit *orderOfDominantcolor2 = new QLineEdit ("1");
    QObject::connect(radioColorSelectorByDominantcolor2, SIGNAL(clicked(bool)), checkboxDominantcolorSelector2, SLOT(setEnabled(bool)));

    layout->addWidget(radioColorSelectorByDominantcolor2);
    layout->addWidget(checkboxDominantcolorSelector2);
    layout->addWidget(orderOfDominantcolor2);
}

void InitializerSelector::addSelectorByKmeanscolor(QHBoxLayout *layout)
{
    QRadioButton *radioColorSelectorByKmeanscolor = new QRadioButton(tr("Detector based on Kmeanscolor"));
    radioColorSelectorByKmeanscolor->setVisible(true);
    QCheckBox *checkboxKmeanscolorSelector = new QCheckBox(this);
    checkboxKmeanscolorSelector->setEnabled(false);
    QLineEdit *orderOfKmeanscolor = new QLineEdit ("1");
    QObject::connect(radioColorSelectorByKmeanscolor, SIGNAL(clicked(bool)), checkboxKmeanscolorSelector, SLOT(setEnabled(bool)));

    layout->addWidget(radioColorSelectorByKmeanscolor);
    layout->addWidget(checkboxKmeanscolorSelector);
    layout->addWidget(orderOfKmeanscolor);
}

void InitializerSelector::addSelectorByHistogramcolor(QHBoxLayout *layout)
{
    QRadioButton *radioColorSelectorByHistogramcolor = new QRadioButton(tr("Detector based on Histogramcolor"));
    radioColorSelectorByHistogramcolor->setVisible(true);
    QCheckBox *checkboxHistogramcolorSelector = new QCheckBox(this);
    checkboxHistogramcolorSelector->setEnabled(false);
    QLineEdit *orderOfHistogramcolor = new QLineEdit ("1");
    QObject::connect(radioColorSelectorByHistogramcolor, SIGNAL(clicked(bool)), checkboxHistogramcolorSelector, SLOT(setEnabled(bool)));

    layout->addWidget(radioColorSelectorByHistogramcolor);
    layout->addWidget(checkboxHistogramcolorSelector);
    layout->addWidget(orderOfHistogramcolor);
}


