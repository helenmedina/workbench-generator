#ifndef INITIALIZERDETECTOR_H
#define INITIALIZERDETECTOR_H
#include <QDialog>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QGroupBox>


class InitializerSelector : public QDialog
{
    Q_OBJECT

public:
    InitializerSelector(QWidget *parent = 0);



private:

	void addSelectorByDominantcolor(QHBoxLayout *layout);

	void addSelectorByKmeanscolor(QHBoxLayout *layout);

	void addSelectorByHistogramcolor(QHBoxLayout *layout);

    void setUpConfiguration();
    void setUpSelection(QVBoxLayout *layout);
    void setUpSelection2(QVBoxLayout *verticalLayout);
    void addSelectorByGrayThreshold(QHBoxLayout *layout);


private:
    QVBoxLayout *detectByColorGroup;
    QGroupBox *groupBoxColor;
    QPushButton *OKButtom;
    QVBoxLayout *layoutForGroupBoxes;

};
#endif // INITIALIZERDETECTOR_H
