#include "lineselector.h"
#include <QRadioButton>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include <QRadioButton>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGraphicsPixmapItem>
#include <QGraphicsView>
#include <QPixmap>
#include <iostream>
#include <QCheckBox>

initializerSelector::initializerSelector(QWidget *parent) : QDialog(parent)
{
    layoutForGroupBoxes = new QHBoxLayout;
    verticalLayoutForGroupBoxSide1A = new QVBoxLayout;
    verticalLayoutForGroupBoxSide1B = new QVBoxLayout;
    verticalLayoutForGroupBoxSide2A = new QVBoxLayout;
    verticalLayoutForGroupBoxSide2B = new QVBoxLayout;
    OKButtom = new QPushButton("Ok");
    setUpConfiguration();

    this->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    this->setMinimumSize(870, 750);
    this->setMaximumSize(870, 750);


}

void initializerSelector::setUpConfiguration()
{
    //set up view
    setUpSoccerView();

    //Set up point x, y for every marker
    QString nameBox = "Soccer Field Markers Side A";
    nameBox += QString( 97, ' ' );
    nameBox += "Soccer Field Markers Side B";
    groupBoxSideA = new QGroupBox( nameBox);
   // groupBoxSideB = new QGroupBox( tr("Soccer Field Marker Side B"));

    //configure marker points display for groupBox

    //Group box A
    setUpMarkersSelectionSide1A(verticalLayoutForGroupBoxSide1A);
    setUpMarkersSelectionSide2A(verticalLayoutForGroupBoxSide2A);
    setUpMarkersSelectionSide1B(verticalLayoutForGroupBoxSide1B);
    setUpMarkersSelectionSide2B(verticalLayoutForGroupBoxSide2B);

    QHBoxLayout *layoutSideA = new QHBoxLayout;
    layoutSideA->addLayout(verticalLayoutForGroupBoxSide1A);
    layoutSideA->addLayout(verticalLayoutForGroupBoxSide2A);
    layoutSideA->addLayout(verticalLayoutForGroupBoxSide1B);
    layoutSideA->addLayout(verticalLayoutForGroupBoxSide2B);

    QVBoxLayout *layoutVSideA = new QVBoxLayout;
    layoutVSideA->addLayout(layoutSideA);
    layoutVSideA->addStretch();
    groupBoxSideA->setLayout(layoutVSideA);

//    //Group box B
//    setUpMarkersSelectionSide1B(verticalLayoutForGroupBoxSide1B);
//    setUpMarkersSelectionSide2B(verticalLayoutForGroupBoxSide2B);
//    QHBoxLayout *layoutSideB = new QHBoxLayout;
//    layoutSideB->addLayout(verticalLayoutForGroupBoxSide1B);
//    layoutSideB->addLayout(verticalLayoutForGroupBoxSide2B);

//    QVBoxLayout *layoutVSideB = new QVBoxLayout;
//    layoutVSideB->addLayout(layoutSideB);
//    layoutVSideB->addStretch();
//    groupBoxSideB->setLayout(layoutVSideB);

    layoutForGroupBoxes->addWidget(groupBoxSideA);
//    layoutForGroupBoxes->addWidget(groupBoxSideB);

    //Button
    layoutButton = new QHBoxLayout;
    layoutButton->addStretch();
    layoutButton->addWidget(OKButtom);
    QObject::connect(OKButtom, SIGNAL(clicked(bool)), this, SLOT(closeDialog()));

    //configure view settings
    layoutView = new QHBoxLayout;
    layoutView->addStretch();
    layoutView->addWidget(view);
    layoutView->addStretch();

    //display in dialog
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addLayout(layoutView);
    mainLayout->addLayout(layoutForGroupBoxes);
    mainLayout->addLayout(layoutButton);

    this->setLayout(mainLayout);


}

void initializerSelector::setUpSoccerView()
{
    scene = new QGraphicsScene(this);
    QImage image;
    image.load("C:/work/soccerAnalyzerDemo/v3/Source/soccerAnalyzerDemo/images_new/soccerField.png");
    QGraphicsPixmapItem *item = new QGraphicsPixmapItem(QPixmap::fromImage(image));
    scene->addItem(item);
    view = new QGraphicsView(scene);
    view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    width = image.width();
    height = image.height();
    view->setMaximumSize(width, height);
    view->fitInView(scene->sceneRect() , Qt::KeepAspectRatioByExpanding);
}

void initializerSelector::configureLineEdit(QLineEdit *pointValue)
{
    pointValue->setFixedWidth(30);
    pointValue->setMaximumWidth(100);
    pointValue->setDisabled(true);
}

void initializerSelector::getPointValue(const QString &nameItem, sPoints &point)
{

    if (nameItem == "Point 1A")
    {
        point.pointID = PointsField::POINT1A;
        point.pointImageTemplate.setX(13);
        point.pointImageTemplate.setY(13);
    }
    else if (nameItem == "Point 2A")
    {
        point.pointID = PointsField::POINT2A;
        point.pointImageTemplate.setX(13);
        point.pointImageTemplate.setY(85);

    }
    else if (nameItem == "Point 3A")
    {
        point.pointID = PointsField::POINT3A;
        point.pointImageTemplate.setX(13);
        point.pointImageTemplate.setY(140);

    }
    else if (nameItem == "Point 4A")
    {
        point.pointID = PointsField::POINT4A;
        point.pointImageTemplate.setX(13);
        point.pointImageTemplate.setY(233);

    }
    else if (nameItem == "Point 5A")
    {
        point.pointID = PointsField::POINT5A;
        point.pointImageTemplate.setX(13);
        point.pointImageTemplate.setY(288);

    }
    else if (nameItem == "Point 6A")
    {
        point.pointID = PointsField::POINT6A;
        point.pointImageTemplate.setX(13);
        point.pointImageTemplate.setY(360);

    }
    else if (nameItem == "Point 7A")
    {
        point.pointID = PointsField::POINT7A;
        point.pointImageTemplate.setX(41);
        point.pointImageTemplate.setY(140);

    }
    else if (nameItem == "Point 8A")
    {
        point.pointID = PointsField::POINT8A;
        point.pointImageTemplate.setX(41);
        point.pointImageTemplate.setY(234);

    }
    else if (nameItem == "Point 9A")
    {
        point.pointID = PointsField::POINT9A;
        point.pointImageTemplate.setX(103);
        point.pointImageTemplate.setY(85);

    }
    else if (nameItem == "Point 10A")
    {
        point.pointID = PointsField::POINT10A;
        point.pointImageTemplate.setX(103);
        point.pointImageTemplate.setY(150);

    }
    else if (nameItem == "Point 11A")
    {
        point.pointID = PointsField::POINT11A;
        point.pointImageTemplate.setX(103);
        point.pointImageTemplate.setY(226);

    }
    else if (nameItem == "Point 12A")
    {
        point.pointID = PointsField::POINT12A;
        point.pointImageTemplate.setX(103);
        point.pointImageTemplate.setY(287);

    }
    else if (nameItem == "Point 13A")
    {
        point.pointID = PointsField::POINT13A;
        point.pointImageTemplate.setX(310);
        point.pointImageTemplate.setY(13);

    }
    else if (nameItem == "Point 14A")
    {
        point.pointID = PointsField::POINT14A;
        point.pointImageTemplate.setX(310);
        point.pointImageTemplate.setY(137);

    }
    else if (nameItem == "Point 15A")
    {
        point.pointID = PointsField::POINT15A;
        point.pointImageTemplate.setX(262);
        point.pointImageTemplate.setY(187);

    }
    else if (nameItem == "Point 1B")
    {
        point.pointID = PointsField::POINT1B;
        point.pointImageTemplate.setX(610);
        point.pointImageTemplate.setY(13);

    }
    else if (nameItem == "Point 2B")
    {
        point.pointID = PointsField::POINT2B;
        point.pointImageTemplate.setX(610);
        point.pointImageTemplate.setY(85);

    }
    else if (nameItem == "Point 3B")
    {
        point.pointID = PointsField::POINT3B;
        point.pointImageTemplate.setX(610);
        point.pointImageTemplate.setY(140);

    }
    else if (nameItem == "Point 4B")
    {
        point.pointID = PointsField::POINT4B;
        point.pointImageTemplate.setX(610);
        point.pointImageTemplate.setY(233);

    }
    else if (nameItem == "Point 5B")
    {
        point.pointID = PointsField::POINT5B;
        point.pointImageTemplate.setX(610);
        point.pointImageTemplate.setY(288);

    }
    else if (nameItem == "Point 6B")
    {
        point.pointID = PointsField::POINT6B;
        point.pointImageTemplate.setX(610);
        point.pointImageTemplate.setY(360);

    }
    else if (nameItem == "Point 7B")
    {
        point.pointID = PointsField::POINT7B;
        point.pointImageTemplate.setX(582);
        point.pointImageTemplate.setY(140);

    }
    else if (nameItem == "Point 8B")
    {
        point.pointID = PointsField::POINT8B;
        point.pointImageTemplate.setX(582);
        point.pointImageTemplate.setY(233);

    }
    else if (nameItem == "Point 9B")
    {
        point.pointID = PointsField::POINT9B;
        point.pointImageTemplate.setX(521);
        point.pointImageTemplate.setY(85);

    }
    else if (nameItem == "Point 10B")
    {
        point.pointID = PointsField::POINT10B;
        point.pointImageTemplate.setX(521);
        point.pointImageTemplate.setY(150);

    }
    else if (nameItem == "Point 11B")
    {
        point.pointID = PointsField::POINT11B;
        point.pointImageTemplate.setX(521);
        point.pointImageTemplate.setY(225);

    }
    else if (nameItem == "Point 12B")
    {
        point.pointID = PointsField::POINT12B;
        point.pointImageTemplate.setX(521);
        point.pointImageTemplate.setY(288);

    }
    else if (nameItem == "Point 13B")
    {
        point.pointID = PointsField::POINT13B;
        point.pointImageTemplate.setX(310);
        point.pointImageTemplate.setY(359);

    }
    else if (nameItem == "Point 14B")
    {
        point.pointID = PointsField::POINT14B;
        point.pointImageTemplate.setX(310);
        point.pointImageTemplate.setY(236);

    }
    else if (nameItem == "Point 15B")
    {
        point.pointID = PointsField::POINT14B;
        point.pointImageTemplate.setX(362);
        point.pointImageTemplate.setY(187);

    }
    //leftx 1051, y750
    //rigth 1450, y 750
}

void initializerSelector::checkItemWhenPointIsSelected(QLayout *layout)
{
    for (int i = 0; i < layout->count(); ++i)
    {
        QWidget *widget = layout->itemAt(i)->widget();
        QCheckBox* itemCheckBox = dynamic_cast<QCheckBox*>(widget);

        if (itemCheckBox != NULL)
        {
            itemCheckBox->setChecked(true);
            break;
        }

    }
}

bool initializerSelector::getPointFromItemSelected(QVBoxLayout* layoutBox, sPoints &point)
{
    bool bFound = false;
    bool bIsChecked = false;
    QList<QLayout *> listLayout = layoutBox -> findChildren<QLayout *>();


    for(int i = 0; i < listLayout.size(); ++i)
    {
        QLayout *widgetContainer = listLayout[i];

        for (int j = 0; j < widgetContainer->count(); ++j)
        {
          QWidget *widget = widgetContainer->itemAt(j)->widget();
          QRadioButton* item = dynamic_cast<QRadioButton*>(widget);
          if (item != NULL)
          {
              QString name;
              name = item->text();
              bIsChecked = item->isChecked();

              if (bIsChecked)
              {

                  getPointValue(name, point);
                  bFound = true;
                  checkItemWhenPointIsSelected(widgetContainer);

                  return bFound;

              }

          }
        }

    }

    return bFound;
}

void initializerSelector::resetCheckboxItem(QVBoxLayout* layoutBox)
{
    QList<QLayout *> listLayout = layoutBox -> findChildren<QLayout *>();

    for(int i = 0; i < listLayout.size(); ++i)
    {
        QLayout *widgetContainer = listLayout[i];

        for (int j = 0; j < widgetContainer->count(); ++j)
        {
          QWidget *widget = widgetContainer->itemAt(j)->widget();
          QCheckBox* item = dynamic_cast<QCheckBox*>(widget);
          if (item != NULL)
          {
              item->setChecked(false);
              item->setDisabled(true);
              break;
          }

         }

    }
}


void initializerSelector::resetSelectionMarkers()
{
    //reset all the markers

    resetCheckboxItem(verticalLayoutForGroupBoxSide1A);
    resetCheckboxItem(verticalLayoutForGroupBoxSide2A);
    resetCheckboxItem(verticalLayoutForGroupBoxSide1B);
    resetCheckboxItem(verticalLayoutForGroupBoxSide2B);


}

sPoints initializerSelector::getPointSelected()
{
    sPoints point;
    point.pointImageTemplate.setX(0);
    point.pointImageTemplate.setY(0);

    if (getPointFromItemSelected(verticalLayoutForGroupBoxSide1A, point))
        return point;

    else if (getPointFromItemSelected(verticalLayoutForGroupBoxSide2A, point))
        return point;

    else if (getPointFromItemSelected(verticalLayoutForGroupBoxSide1B, point))
        return point;

    else if (getPointFromItemSelected(verticalLayoutForGroupBoxSide2B, point))
        return point;

    return point;
}

void initializerSelector::closeDialog()
{
    emit checkPointsSelectorRule();

}

void initializerSelector::setUpMarkersSelectionSide1A(QVBoxLayout *verticalLayout)
{
    verticalLayout->setSizeConstraint(QLayout::SetFixedSize);
    //Create and add point 1 to layout
    QHBoxLayout *groupBoxLayoutPoint1A = new QHBoxLayout;
    //groupBoxLayoutPoint1A->setSizeConstraint(QLayout::SetFixedSize);
    addPoint1A(groupBoxLayoutPoint1A);

    QHBoxLayout *groupBoxLayoutPoint2A = new QHBoxLayout;

    addPoint2A(groupBoxLayoutPoint2A);

    QHBoxLayout *groupBoxLayoutPoint3A = new QHBoxLayout;

    addPoint3A(groupBoxLayoutPoint3A);

    QHBoxLayout *groupBoxLayoutPoint4A = new QHBoxLayout;
    addPoint4A(groupBoxLayoutPoint4A);

    QHBoxLayout *groupBoxLayoutPoint5A = new QHBoxLayout;
    addPoint5A(groupBoxLayoutPoint5A);

    QHBoxLayout *groupBoxLayoutPoint6A = new QHBoxLayout;
    addPoint6A(groupBoxLayoutPoint6A);

    QHBoxLayout *groupBoxLayoutPoint7A = new QHBoxLayout;
    addPoint7A(groupBoxLayoutPoint7A);

    QHBoxLayout *groupBoxLayoutPoint8A = new QHBoxLayout;
    addPoint8A(groupBoxLayoutPoint8A);

    verticalLayout->addLayout(groupBoxLayoutPoint1A);
    verticalLayout->addLayout(groupBoxLayoutPoint2A);
    verticalLayout->addLayout(groupBoxLayoutPoint3A);
    verticalLayout->addLayout(groupBoxLayoutPoint4A);
    verticalLayout->addLayout(groupBoxLayoutPoint5A);
    verticalLayout->addLayout(groupBoxLayoutPoint6A);
    verticalLayout->addLayout(groupBoxLayoutPoint7A);
    verticalLayout->addLayout(groupBoxLayoutPoint8A);

}

void initializerSelector::setUpMarkersSelectionSide2A(QVBoxLayout *verticalLayout)
{
    //Create and add point 1 to layout
verticalLayout->setSizeConstraint(QLayout::SetFixedSize);


    QHBoxLayout *groupBoxLayoutPoint9A = new QHBoxLayout;
    addPoint9A(groupBoxLayoutPoint9A);

    QHBoxLayout *groupBoxLayoutPoint10A = new QHBoxLayout;
    addPoint10A(groupBoxLayoutPoint10A);

    QHBoxLayout *groupBoxLayoutPoint11A = new QHBoxLayout;
    addPoint11A(groupBoxLayoutPoint11A);

    QHBoxLayout *groupBoxLayoutPoint12A = new QHBoxLayout;
    addPoint12A(groupBoxLayoutPoint12A);

    QHBoxLayout *groupBoxLayoutPoint13A = new QHBoxLayout;
    addPoint13A(groupBoxLayoutPoint13A);

    QHBoxLayout *groupBoxLayoutPoint14A = new QHBoxLayout;
    addPoint14A(groupBoxLayoutPoint14A);

    QHBoxLayout *groupBoxLayoutPoint15A = new QHBoxLayout;
    addPoint15A(groupBoxLayoutPoint15A);

    //verticalLayout->addLayout(groupBoxLayoutPoint8A);
    verticalLayout->addLayout(groupBoxLayoutPoint9A);
    verticalLayout->addLayout(groupBoxLayoutPoint10A);
    verticalLayout->addLayout(groupBoxLayoutPoint11A);
    verticalLayout->addLayout(groupBoxLayoutPoint12A);
    verticalLayout->addLayout(groupBoxLayoutPoint13A);
    verticalLayout->addLayout(groupBoxLayoutPoint14A);
    verticalLayout->addLayout(groupBoxLayoutPoint15A);
}

void initializerSelector::setUpMarkersSelectionSide1B(QVBoxLayout *verticalLayout)
{
    verticalLayout->setSizeConstraint(QLayout::SetFixedSize);
    QHBoxLayout *groupBoxLayoutPoint1B = new QHBoxLayout;
    groupBoxLayoutPoint1B->setStretch(0,1);
    addPoint1B(groupBoxLayoutPoint1B);

    QHBoxLayout *groupBoxLayoutPoint2B = new QHBoxLayout;
    groupBoxLayoutPoint2B->setStretch(0,1);
    addPoint2B(groupBoxLayoutPoint2B);

    QHBoxLayout *groupBoxLayoutPoint3B = new QHBoxLayout;
    addPoint3B(groupBoxLayoutPoint3B);

    QHBoxLayout *groupBoxLayoutPoint4B = new QHBoxLayout;
    addPoint4B(groupBoxLayoutPoint4B);

    QHBoxLayout *groupBoxLayoutPoint5B = new QHBoxLayout;
    addPoint5B(groupBoxLayoutPoint5B);

    QHBoxLayout *groupBoxLayoutPoint6B = new QHBoxLayout;
    addPoint6B(groupBoxLayoutPoint6B);

    QHBoxLayout *groupBoxLayoutPoint7B = new QHBoxLayout;
    addPoint7B(groupBoxLayoutPoint7B);

    QHBoxLayout *groupBoxLayoutPoint8B = new QHBoxLayout;
    addPoint8B(groupBoxLayoutPoint8B);


    verticalLayout->addLayout(groupBoxLayoutPoint1B);
    verticalLayout->addLayout(groupBoxLayoutPoint2B);
    verticalLayout->addLayout(groupBoxLayoutPoint3B);
    verticalLayout->addLayout(groupBoxLayoutPoint4B);
    verticalLayout->addLayout(groupBoxLayoutPoint5B);
    verticalLayout->addLayout(groupBoxLayoutPoint6B);
    verticalLayout->addLayout(groupBoxLayoutPoint7B);
    verticalLayout->addLayout(groupBoxLayoutPoint8B);

}

void initializerSelector::setUpMarkersSelectionSide2B(QVBoxLayout *verticalLayout)
{
verticalLayout->setSizeConstraint(QLayout::SetFixedSize);

    QHBoxLayout *groupBoxLayoutPoint9B = new QHBoxLayout;
    addPoint9B(groupBoxLayoutPoint9B);

    QHBoxLayout *groupBoxLayoutPoint10B = new QHBoxLayout;
    addPoint10B(groupBoxLayoutPoint10B);

    QHBoxLayout *groupBoxLayoutPoint11B = new QHBoxLayout;
    addPoint11B(groupBoxLayoutPoint11B);

    QHBoxLayout *groupBoxLayoutPoint12B = new QHBoxLayout;
    addPoint12B(groupBoxLayoutPoint12B);

    QHBoxLayout *groupBoxLayoutPoint13B = new QHBoxLayout;
    addPoint13B(groupBoxLayoutPoint13B);

    QHBoxLayout *groupBoxLayoutPoint14B = new QHBoxLayout;
    addPoint14B(groupBoxLayoutPoint14B);

    QHBoxLayout *groupBoxLayoutPoint15B = new QHBoxLayout;
    addPoint15B(groupBoxLayoutPoint15B);

   // verticalLayout->addLayout(groupBoxLayoutPoint8B);
    verticalLayout->addLayout(groupBoxLayoutPoint9B);
    verticalLayout->addLayout(groupBoxLayoutPoint10B);
    verticalLayout->addLayout(groupBoxLayoutPoint11B);
    verticalLayout->addLayout(groupBoxLayoutPoint12B);
    verticalLayout->addLayout(groupBoxLayoutPoint13B);
    verticalLayout->addLayout(groupBoxLayoutPoint14B);
    verticalLayout->addLayout(groupBoxLayoutPoint15B);
}

void initializerSelector::addPoint1A(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point1A = new QRadioButton(tr("Point 1A"));
    QCheckBox *checkbox1A = new QCheckBox(this);
    checkbox1A->setEnabled(false);
    QObject::connect(point1A, SIGNAL(clicked(bool)), checkbox1A, SLOT(setEnabled(bool)));
    //point1A->setChecked(true);
    groupBoxLayout->addWidget(point1A);
    groupBoxLayout->addWidget(checkbox1A);

}

void initializerSelector::addPoint2A(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point2A = new QRadioButton(tr("Point 2A"));
    QCheckBox *checkbox2A = new QCheckBox(this);
    checkbox2A->setEnabled(false);
    QObject::connect(point2A, SIGNAL(clicked(bool)), checkbox2A, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point2A);
    groupBoxLayout->addWidget(checkbox2A);
}

void initializerSelector::addPoint3A(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point3A = new QRadioButton(tr("Point 3A"));
    QCheckBox *checkbox3A = new QCheckBox(this);
    checkbox3A->setEnabled(false);
    QObject::connect(point3A, SIGNAL(clicked(bool)), checkbox3A, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point3A);
    groupBoxLayout->addWidget(checkbox3A);
}

void initializerSelector::addPoint4A(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point4A = new QRadioButton(tr("Point 4A"));
    QCheckBox *checkbox4A = new QCheckBox(this);
    checkbox4A->setEnabled(false);
    QObject::connect(point4A, SIGNAL(clicked(bool)), checkbox4A, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point4A);
    groupBoxLayout->addWidget(checkbox4A);
}

void initializerSelector::addPoint5A(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point5A = new QRadioButton(tr("Point 5A"));
    QCheckBox *checkbox5A = new QCheckBox(this);
    checkbox5A->setEnabled(false);
    QObject::connect(point5A, SIGNAL(clicked(bool)), checkbox5A, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point5A);
    groupBoxLayout->addWidget(checkbox5A);
}

void initializerSelector::addPoint6A(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point6A = new QRadioButton(tr("Point 6A"));
    QCheckBox *checkbox6A = new QCheckBox(this);
    checkbox6A->setEnabled(false);
    QObject::connect(point6A, SIGNAL(clicked(bool)), checkbox6A, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point6A);
    groupBoxLayout->addWidget(checkbox6A);
}
void initializerSelector::addPoint7A(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point7A = new QRadioButton(tr("Point 7A"));
    QCheckBox *checkbox7A = new QCheckBox(this);
    checkbox7A->setEnabled(false);
    QObject::connect(point7A, SIGNAL(clicked(bool)), checkbox7A, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point7A);
    groupBoxLayout->addWidget(checkbox7A);
}

void initializerSelector::addPoint8A(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point8A = new QRadioButton(tr("Point 8A"));
    QCheckBox *checkbox8A = new QCheckBox(this);
    checkbox8A->setEnabled(false);
    QObject::connect(point8A, SIGNAL(clicked(bool)), checkbox8A, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point8A);
    groupBoxLayout->addWidget(checkbox8A);
}

void initializerSelector::addPoint9A(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point9A = new QRadioButton(tr("Point 9A"));
    QCheckBox *checkbox9A = new QCheckBox(this);
    checkbox9A->setEnabled(false);
    QObject::connect(point9A, SIGNAL(clicked(bool)), checkbox9A, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point9A);
    groupBoxLayout->addWidget(checkbox9A);
}

void initializerSelector::addPoint10A(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point10A = new QRadioButton(tr("Point 10A"));
    QCheckBox *checkbox10A = new QCheckBox(this);
    checkbox10A->setEnabled(false);
    QObject::connect(point10A, SIGNAL(clicked(bool)), checkbox10A, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point10A);
    groupBoxLayout->addWidget(checkbox10A);
}

void initializerSelector::addPoint11A(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point11A = new QRadioButton(tr("Point 11A"));
    QCheckBox *checkbox11A = new QCheckBox(this);
    checkbox11A->setEnabled(false);
    QObject::connect(point11A, SIGNAL(clicked(bool)), checkbox11A, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point11A);
    groupBoxLayout->addWidget(checkbox11A);
}

void initializerSelector::addPoint12A(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point12A = new QRadioButton(tr("Point 12A"));
    QCheckBox *checkbox12A = new QCheckBox(this);
    checkbox12A->setEnabled(false);
    QObject::connect(point12A, SIGNAL(clicked(bool)), checkbox12A, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point12A);
    groupBoxLayout->addWidget(checkbox12A);
}

void initializerSelector::addPoint13A(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point13A = new QRadioButton(tr("Point 13A"));
    QCheckBox *checkbox13A = new QCheckBox(this);
    checkbox13A->setEnabled(false);
    QObject::connect(point13A, SIGNAL(clicked(bool)), checkbox13A, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point13A);
    groupBoxLayout->addWidget(checkbox13A);
}

void initializerSelector::addPoint14A(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point14A = new QRadioButton(tr("Point 14A"));
    QCheckBox *checkbox14A = new QCheckBox(this);
    checkbox14A->setEnabled(false);
    QObject::connect(point14A, SIGNAL(clicked(bool)), checkbox14A, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point14A);
    groupBoxLayout->addWidget(checkbox14A);
}


void initializerSelector::addPoint1B(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point1B = new QRadioButton(tr("Point 1B"));
    QCheckBox *checkbox1B = new QCheckBox(this);
    checkbox1B->setEnabled(false);
    QObject::connect(point1B, SIGNAL(clicked(bool)), checkbox1B, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point1B);
    groupBoxLayout->addWidget(checkbox1B);
}

void initializerSelector::addPoint2B(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point2B = new QRadioButton(tr("Point 2B"));
    QCheckBox *checkbox2B = new QCheckBox(this);
    checkbox2B->setEnabled(false);
    QObject::connect(point2B, SIGNAL(clicked(bool)), checkbox2B, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point2B);
    groupBoxLayout->addWidget(checkbox2B);
}

void initializerSelector::addPoint3B(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point3B = new QRadioButton(tr("Point 3B"));
    QCheckBox *checkbox3B = new QCheckBox(this);
    checkbox3B->setEnabled(false);
    QObject::connect(point3B, SIGNAL(clicked(bool)), checkbox3B, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point3B);
    groupBoxLayout->addWidget(checkbox3B);
}

void initializerSelector::addPoint4B(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point4B = new QRadioButton(tr("Point 4B"));
    QCheckBox *checkbox4B = new QCheckBox(this);
    checkbox4B->setEnabled(false);
    QObject::connect(point4B, SIGNAL(clicked(bool)), checkbox4B, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point4B);
    groupBoxLayout->addWidget(checkbox4B);
}

void initializerSelector::addPoint5B(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point5B = new QRadioButton(tr("Point 5B"));
    QCheckBox *checkbox5B = new QCheckBox(this);
    checkbox5B->setEnabled(false);
    QObject::connect(point5B, SIGNAL(clicked(bool)), checkbox5B, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point5B);
    groupBoxLayout->addWidget(checkbox5B);
}

void initializerSelector::addPoint6B(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point6B = new QRadioButton(tr("Point 6B"));
    QCheckBox *checkbox6B = new QCheckBox(this);
    checkbox6B->setEnabled(false);
    QObject::connect(point6B, SIGNAL(clicked(bool)), checkbox6B, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point6B);
    groupBoxLayout->addWidget(checkbox6B);
}
void initializerSelector::addPoint7B(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point7B = new QRadioButton(tr("Point 7B"));
    QCheckBox *checkbox7B = new QCheckBox(this);
    checkbox7B->setEnabled(false);
    QObject::connect(point7B, SIGNAL(clicked(bool)), checkbox7B, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point7B);
    groupBoxLayout->addWidget(checkbox7B);
}

void initializerSelector::addPoint8B(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point8B = new QRadioButton(tr("Point 8B"));
    QCheckBox *checkbox8B = new QCheckBox(this);
    checkbox8B->setEnabled(false);
    QObject::connect(point8B, SIGNAL(clicked(bool)), checkbox8B, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point8B);
    groupBoxLayout->addWidget(checkbox8B);
}

void initializerSelector::addPoint9B(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point9B = new QRadioButton(tr("Point 9B"));
    QCheckBox *checkbox9B = new QCheckBox(this);
    checkbox9B->setEnabled(false);
    QObject::connect(point9B, SIGNAL(clicked(bool)), checkbox9B, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point9B);
    groupBoxLayout->addWidget(checkbox9B);
}

void initializerSelector::addPoint10B(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point10B = new QRadioButton(tr("Point 10B"));
    QCheckBox *checkbox10B = new QCheckBox(this);
    checkbox10B->setEnabled(false);
    QObject::connect(point10B, SIGNAL(clicked(bool)), checkbox10B, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point10B);
    groupBoxLayout->addWidget(checkbox10B);
}

void initializerSelector::addPoint11B(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point11B = new QRadioButton(tr("Point 11B"));
    QCheckBox *checkbox11B = new QCheckBox(this);
    checkbox11B->setEnabled(false);
    QObject::connect(point11B, SIGNAL(clicked(bool)), checkbox11B, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point11B);
    groupBoxLayout->addWidget(checkbox11B);
}

void initializerSelector::addPoint12B(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point12B = new QRadioButton(tr("Point 12B"));
    QCheckBox *checkbox12B = new QCheckBox(this);
    checkbox12B->setEnabled(false);
    QObject::connect(point12B, SIGNAL(clicked(bool)), checkbox12B, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point12B);
    groupBoxLayout->addWidget(checkbox12B);
}

void initializerSelector::addPoint13B(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point13B = new QRadioButton(tr("Point 13B"));
    QCheckBox *checkbox13B = new QCheckBox(this);
    checkbox13B->setEnabled(false);
    QObject::connect(point13B, SIGNAL(clicked(bool)), checkbox13B, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point13B);
    groupBoxLayout->addWidget(checkbox13B);
}

void initializerSelector::addPoint14B(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point14B = new QRadioButton(tr("Point 14B"));
    QCheckBox *checkbox14B = new QCheckBox(this);
    checkbox14B->setEnabled(false);
    QObject::connect(point14B, SIGNAL(clicked(bool)), checkbox14B, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point14B);
    groupBoxLayout->addWidget(checkbox14B);
}

void initializerSelector::addPoint15B(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point15B = new QRadioButton(tr("Point 15B"));
    QCheckBox *checkbox15B = new QCheckBox(this);
    checkbox15B->setEnabled(false);
    QObject::connect(point15B, SIGNAL(clicked(bool)), checkbox15B, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point15B);
    groupBoxLayout->addWidget(checkbox15B);
}

void initializerSelector::addPoint15A(QHBoxLayout *groupBoxLayout)
{
    QRadioButton *point15A = new QRadioButton(tr("Point 15A"));
    QCheckBox *checkbox15A = new QCheckBox(this);
    checkbox15A->setEnabled(false);
    QObject::connect(point15A, SIGNAL(clicked(bool)), checkbox15A, SLOT(setEnabled(bool)));

    groupBoxLayout->addWidget(point15A);
    groupBoxLayout->addWidget(checkbox15A);
}
