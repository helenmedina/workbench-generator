#ifndef RECTANGLEITEM_H
#define RECTANGLEITEM_H

#include <QGraphicsRectItem>
#include "shared/shared.h"

class RectangleItem : public QObject, public QGraphicsRectItem
{

Q_OBJECT

public:

    RectangleItem();
    void setName(QString);
    QString getName();
    void setLabel(QString name);
    QString getLabel();
    void setX(int);
    int getX();
    void setY(int );
    int getY();
    void setWidth(unsigned int);
    unsigned int getWidth();
    void setHeigth(unsigned int);
    unsigned int getHeigth();
    void setColor(sColor);
    sColor getColor();

private:

    QString strName;
    QString strLabel;
    int iValueX;
    int iValueY;
    unsigned int uiWidth;
    unsigned int uiHeigth;
    sColor color;

protected:

    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);

signals:

    void removeScene(const QPointF & point);
    void changeColor(QColor, const QPointF & point);
    void changeAllColor(QColor);
    void addLabel(const QPointF & point);
    void addName(const QPointF & point);
    void modifyObject(const QPointF & point);
};

#endif // RECTANGLEITEM_H
