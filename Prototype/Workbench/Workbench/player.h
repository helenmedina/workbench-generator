#ifndef PLAYER_H
#define PLAYER_H

#include <QWidget>
#include "opencv2/opencv.hpp"

class PlayerWidget : public QWidget
{
    Q_OBJECT

public:

    explicit PlayerWidget(QWidget *parent = 0);
    void play();
    bool loadVideo(QString );
    void getImage(cv::Mat &frame, QImage &image);
    void stop();
    void msleep(int );
    void release();
    void setDelay(int);
    int getDelay();
    long long int getNumberOfFrames();
    long long int getCurrentFrame();
    void setCurrentFrame(long long int);
    cv::VideoCapture& getVideoCapture();
    void updateImage();
    void updateImage(cv::Mat& newFrame);
    void setFrame(cv::Mat&);
    cv::Mat& getFrame();
     ~PlayerWidget();


private:

    const int NUM_FRAMES = 1000;
    QString fileName;
    cv::VideoCapture capture;
    QImage image;
    int m_delay;
    bool m_bStop;
    int currentF;
    cv::Mat currentFrame;


signals:

    //output frame to be displayed in MainWindow
     void processImage(const QImage&);

};

#endif // PLAYER_H
