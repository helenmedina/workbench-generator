#ifndef DOMINANTCOLORDETECTOR_H
#define DOMINANTCOLORDETECTOR_H
#include "ColorDetector.h"

class DominantColorDetector : public ColorDetector
{

public:
    virtual std::vector<cv::Vec3b> getColors(const cv::Mat &image, int count);

};
#endif // DOMINANTCOLORDETECTOR_H
