#ifndef OBJECTDETECTION_H
#define OBJECTDETECTION_H

#include <opencv2/opencv.hpp>
#include <shared/shared.h>

class OjectDetection
{
public:
    OjectDetection();
    void process (cv::Mat &image, cv::Mat &output);
    void setNextProcessor (OjectDetection* p);
    virtual ~OjectDetection(){}
    virtual std::vector<objectDetails> getObjectDetails() = 0;

protected:
    virtual void processImplementation (cv::Mat &image, cv::Mat &output) = 0;


private:
    OjectDetection *nextProcessor;

};

#endif // OBJECTDETECTION_H
