#ifndef HISTOGRAMCOLORDETECTOR_H
#define HISTOGRAMCOLORDETECTOR_H

#include "ColorDetector.h"

class HistogramColorDetector : public ColorDetector
{

public:
    virtual std::vector<cv::Vec3b> getColors(const cv::Mat &image, int count);

};

#endif // HISTOGRAMCOLORDETECTOR_H
