
#ifndef DOMINANT_COLOR_DETECTOR_H
#define DOMINANT_COLOR_DETECTOR_H

#include "shared/shared.h"
#include <vector>
#include <opencv2/opencv.hpp>

class ColorDetector
{

public:
	//count -> number of colors to be detected
    virtual std::vector<cv::Vec3b> getColors(const cv::Mat &image, int count) = 0;

};

#endif //DOMINANT_COLOR_DETECTOR_H

