#ifndef OBJECT_DETECTION_BY_COLOR_METHOD
#define OBJECT_DETECTION_BY_COLOR_METHOD

#include "objectdetection.h"
#include "shared/shared.h"
class objectDetectionByColorMethod : public OjectDetection
{
public:
    virtual std::vector<objectDetails> getObjectDetails();

private:
	void processImplementation(cv::Mat &image, cv::Mat &output);



private:
    std::vector<objectDetails> objectDetails;

};

#endif //OBJECT_DETECTION_BY_COLOR_METHOD
