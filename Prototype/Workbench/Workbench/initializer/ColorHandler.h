#ifndef DOMINANTCOLORHANDLER_H
#define DOMINANTCOLORHANDLER_H

#include "ColorDetector.h"

class ColorHandler
{
    ColorDetector *colorDetector;

public:
    void setColorDetector(ColorDetector*);
    std::vector<cv::Vec3b> detectColors(const cv::Mat &image, int count);
    void processImage(std::vector<cv::Vec3b>, cv::Mat &image);
};

#endif // DOMINANTCOLORHANDLER_H
