#include "ObjectDetectionByColorMethod.h"
#include "kmeanscolordetector.h"
#include "colorhandler.h"

void objectDetectionByColorMethod::processImplementation(cv::Mat &image, cv::Mat &output)
{

    //Here the proposed automatic tool (with ruby embedded) can geenrate the code according the options and input  parameters enter by the user
    kMeansColorDetector kMeansDetector;
    ColorHandler colorHandler;
    colorHandler.setColorDetector(&kMeansDetector);

    int numColors = 3;
    //Detect main colors from image
    std::vector<cv::Vec3b> candidates = colorHandler.detectColors(image, numColors);
    //Process image with colors detected and get mask binary with objects detected according the color selected or dominant color
    //This las part depends from user, so theses options can be generated automatically
    colorHandler.processImage(candidates, image);

}

std::vector<objectDetails> objectDetectionByColorMethod::getObjectDetails()
{
    return this->objectDetails;
}
