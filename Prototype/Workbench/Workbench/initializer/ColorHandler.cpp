#include "ColorHandler.h"

void ColorHandler::setColorDetector(ColorDetector* detector)
{
    this->colorDetector = detector;
}

std::vector<cv::Vec3b> ColorHandler::detectColors(const cv::Mat &image, int count)
{
    return colorDetector->getColors(image, count);
}

void ColorHandler::processImage(std::vector<cv::Vec3b> colors, cv::Mat &image)
{

}
