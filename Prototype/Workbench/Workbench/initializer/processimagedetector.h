#ifndef OBJECT_IMAGE_DETECTOR_H
#define OBJECT_IMAGE_DETECTOR_H

#include "objectdetection.h"

class ProcessImageDetector : public OjectDetection
{
public:
    std::vector<objectDetails> getObjectDetails();

private:
    void processImplementation(cv::Mat &image, cv::Mat &output);

private:
    std::vector<objectDetails> objectDetails;
};

#endif // OBJECT_IMAGE_DETECTOR_H
