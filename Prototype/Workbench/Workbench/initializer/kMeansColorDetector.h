#ifndef KMEANSCOLORDETECTOR_H
#define KMEANSCOLORDETECTOR_H

#include "ColorDetector.h"

class kMeansColorDetector : public ColorDetector
{

public:
    virtual std::vector<cv::Vec3b> getColors(const cv::Mat &image, int count);

};


#endif // KMEANSCOLORDETECTOR_H
