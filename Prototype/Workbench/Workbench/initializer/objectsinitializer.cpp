#include "initializer/objectsinitializer.h"
#include "ColorHandler.h"
#include "processimagedetector.h"
#include "ObjectDetectionByColorMethod.h"
#include "shared/utilities.h"
#include <QProgressDialog>
#include <QTimer>

objectsInitializer::objectsInitializer()
{}

bool objectsInitializer::detectObjects(cv::Mat &frame, std::vector<objectDetails>& objectsDetected)
{

    emit InitializerPocessFinished(false);

    if (!frame.empty())
    {
        cv::Mat output;
        //start process
        ProcessImageDetector processImageDetector;
        objectDetectionByColorMethod detectionByColor;
        processImageDetector.setNextProcessor(&detectionByColor);

        processImageDetector.process(frame, output);

        //Get the objects detected
        objectsDetected = detectionByColor.getObjectDetails();

   }


    emit InitializerPocessFinished(true);

    if (objectsDetected.empty())
        return false;
    else
        return true;
}


