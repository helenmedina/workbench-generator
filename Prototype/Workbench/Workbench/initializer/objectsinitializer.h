#ifndef OBJECTS_INITIALIZER_H
#define OBJECTS_INITIALIZER_H

#include <QObject>
#include <opencv2/opencv.hpp>
#include "shared/shared.h"

class objectsInitializer : public QObject
{
    Q_OBJECT

public:
    objectsInitializer();
    bool detectObjects(cv::Mat&, std::vector<objectDetails>&);


signals:
    void InitializerPocessFinished(bool);

public slots:


};

#endif // OBJECTS_INITIALIZER_H
