#ifndef SHARED_H
#define SHARED_H

#include <string>

struct sColor
{
    int red;
    int green;
    int blue;
};

struct playerDetails
{
    int x;
    int y;
    int w;
    int h;
    std::string playerName;
    sColor playerColor;
    int numberFrame;
};

#endif // SHARED_H
