#ifndef OBJECTTRACKER_H
#define OBJECTTRACKER_H

#include <shared/shared.h>
#include <QMutex>
#include <kcftracker.hpp>


class ObjectTracker
{
public:
    ObjectTracker(bool hog = true, bool fixed_window = true, bool multiscale = true, bool lab = true);
    void setInitialPosition(objectDetails, cv::Mat &, std::vector<objectDetails> &objectsDetected);
    void update(cv::Mat &, std::vector<objectDetails>&, QMutex &mutexWorkers);
    void setObjectName(std::string);
    void setColor(sColor);
    void setGeneralParameters(std::string, std::string, sColor);
    void setObjectLabel(std::string);
    void destroy();

private:
    KCFTracker *tracker;
    std::string objectName;
    std::string objectLabel;
    sColor color;
    long long int numFrame;

};

#endif // OBJECTTRACKER_H
