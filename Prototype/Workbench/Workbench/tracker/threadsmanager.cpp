#include <tracker/threadsmanager.h>
#include <QFuture>
#include <QtConcurrent>
#include <QFutureSynchronizer>
#include <iostream>


 ThreadsManager::ThreadsManager()
 {
     bstop = false;
     object = nullptr;
 }

void ThreadsManager::run()
{

    cv::Mat frame;
    QMutex mutex;

    while(!isCanceled())
    {
        //Reset the objectsDetected container for the new frame
        objectsTracked.clear();
        capture >> frame;
        if (!frame.empty())
        {
            try
            {
                setNumberFrame(capture.get(CV_CAP_PROP_POS_FRAMES));

                QFutureSynchronizer<void> synchronizer;
                //Creates a thread for every object
                for (int i = 0; i < numObjects; i++)
                {
                  synchronizer.addFuture(QtConcurrent::run( object[i], &ObjectTracker::update, std::ref(frame),std::ref(objectsTracked), std::ref(mutex)));
                }

                synchronizer.waitForFinished();
                synchronizer.clearFutures();
                //Wait for all the threads to finish before saving data of the object or send it to the controller
                saveData(currentFrame);
                //Sends the new positions of the objects to the controller
                emit readyToDisplay(objectsTracked, currentFrame - 1);

            }
            catch(...)
            {
                std::string messageError = "An error has ocurred during tracking process.";
                emit errorLabelObject(messageError);
            }

        }
        else
            stopTracking(true);

     }

    //Release KFCTracker memory
    for (int i = 0; i < numObjects; i++)
    {
        object[i]->destroy();
    }

}

void ThreadsManager::saveData(long long int numberFrame)
{
    //Save information and position of the object in a text file
    std::ofstream output;
    std::string filename;
    std::ostringstream numberFileToPrint;

    numberFileToPrint << numberFrame;
    filename = filenamePath + numberFileToPrint.str() + ".txt";

    output.open(filename, std::fstream::app);

    for (int i = 0; i < objectsTracked.size(); i++)
    {
        objectDetails object = objectsTracked[i];

        output << "[" << object.objectLabel<< "]"<< std::endl;
        output << "nombre = " << object.objectName<< std::endl;
        output << "pos_x = " << object.x << std::endl;
        output << "pos_y = " << object.y << std::endl;
    }

}


void ThreadsManager::stopTracking(bool isStop)
{
    const QMutexLocker locker (&mutexCanceled);
    bstop = isStop;
}

bool ThreadsManager::isCanceled()
{
    const QMutexLocker locker (&mutexCanceled);
    return bstop;
}

void ThreadsManager::setCapture(cv::VideoCapture videoCapture)
{
    capture = videoCapture;
}

void ThreadsManager::setNumberFrame(long long int value)
{
    currentFrame = value;
}

void ThreadsManager::setNumberObjects(int value)
{
    numObjects = value;
}

bool ThreadsManager::createObjects(int numberObjectsDetected, const std::vector<objectDetails>& ObjectsDetected)
{

    //Creates the object before linking in a thread
    cv::Mat initFrame;
    capture >> initFrame;

    if (initFrame.empty())
        return false;

    setNumberFrame(capture.get(CV_CAP_PROP_POS_FRAMES));
    setNumberObjects(numberObjectsDetected);

    object = new ObjectTracker*[numberObjectsDetected];
    for (int i = 0; i < numberObjectsDetected; i++)
    {
        objectDetails initialPosition = ObjectsDetected[i];
        object[i] = new ObjectTracker();
        object[i]->setInitialPosition(initialPosition, initFrame, objectsTracked);

    }

    saveData(currentFrame);

    return true;
}

void ThreadsManager::setFileNamePath(std::string path)
{
    this->filenamePath = path;
}

ThreadsManager::~ThreadsManager()
{

    if (object != nullptr)
    {
        for (int i = 0; i < numObjects; i++)
        {
            delete object[i];
            object[i] = nullptr;
        }

        delete []object;
    }

}
