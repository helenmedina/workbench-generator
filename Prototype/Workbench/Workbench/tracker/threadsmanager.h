#ifndef THREADSMANAGER_H
#define THREADSMANAGER_H

#include <tracker/objectracker.h>
#include <opencv2/opencv.hpp>
#include <QThread>


class ThreadsManager : public QThread{

    Q_OBJECT

public:

    ThreadsManager();
    ~ThreadsManager();
    void run();
    void setCapture(cv::VideoCapture vd);
    void setNumberFrame(long long int);
    void setNumberObjects(int);
    bool isCanceled();
    bool createObjects(int , const std::vector<objectDetails>&);
    void saveData(long long int numberFrame);
    void setFileNamePath(std::string);


signals:

   void readyToDisplay(const std::vector<objectDetails>&, const long long int&);
   void errorLabelObject(std::string&);

public slots:

   void stopTracking(bool);

private:

    bool bstop;
    cv::VideoCapture capture;
    std::vector<objectDetails> objectsTracked;
    long long int currentFrame;
    int numObjects;
    mutable QMutex mutexCanceled;
    ObjectTracker **object;
    std::string filenamePath;

};

#endif // THREADSMANAGER_H
