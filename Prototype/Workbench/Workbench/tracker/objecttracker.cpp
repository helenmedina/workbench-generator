#include <tracker/objectracker.h>
#include <iostream>

ObjectTracker::ObjectTracker(bool hog, bool fixed_window, bool multiscale, bool lab)
{
    tracker = new KCFTracker(hog, fixed_window, multiscale, lab);
    color.red = 0;
    color.green = 255;
    color.blue = 0;
}

void ObjectTracker::setInitialPosition(objectDetails initialPosition, cv::Mat &initialFrame, std::vector<objectDetails> &objectsDetected)
{
    setGeneralParameters(initialPosition.objectName, initialPosition.objectLabel, initialPosition.objectColor);
    tracker->init(cv::Rect(initialPosition.x, initialPosition.y, initialPosition.w, initialPosition.h), initialFrame);
    numFrame = initialPosition.numberFrame;

    //save object details in objectsDetected container to render it in UI
    objectsDetected.push_back(initialPosition);

}

void ObjectTracker::setObjectName(std::string name)
{
    objectName = name;
}

void ObjectTracker::setObjectLabel(std::string label)
{
    objectLabel = label;
}


void ObjectTracker::setColor(sColor colorObject)
{
    color = colorObject;
}

void ObjectTracker::setGeneralParameters(std::string name, std::string label, sColor color)
{
    setObjectName(name);
    setObjectLabel(label);
    setColor(color);
}

//Get the positions of the object from the new image
void ObjectTracker::update(cv::Mat &frame, std::vector<objectDetails> &objectsDetected, QMutex &mutexWorkers)
{

    try {

        cv::Rect result = tracker->update(frame);
        numFrame++;
        objectDetails objectDetected;
        objectDetected.x = result.x;
        objectDetected.y = result.y;
        objectDetected.w = result.width;
        objectDetected.h = result.height;
        objectDetected.objectName = this->objectName;
        objectDetected.objectLabel = this->objectLabel;
        objectDetected.objectColor = this->color;
        objectDetected.numberFrame = this->numFrame;
        mutexWorkers.lock();

        objectsDetected.push_back(objectDetected);

        mutexWorkers.unlock();

    }
    catch (std::logic_error& e)
    {
        std::cout << e.what();
    }
    catch(...)
    {
        std::cout << "Error unknown in objectTracker::update";
    }

}

void ObjectTracker::destroy()
{
    delete tracker;
    tracker = nullptr;
}
