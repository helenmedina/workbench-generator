#include "rectangleitem.h"
#include <QMenu>
#include <QGraphicsSceneContextMenuEvent>
#include <QColorDialog>


RectangleItem::RectangleItem()
{
    iValueX = 0;
    iValueY = 0;
    uiWidth = 0;
    uiHeigth = 0;
}

void RectangleItem::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    QMenu menu;

    QAction *modifyAction = menu.addAction("Modify");
    QAction *removeAction = menu.addAction("Remove");
    QAction *labelObjectAction = menu.addAction("Set label object");
    QAction *NameObjectAction = menu.addAction("Set name object");
    QAction *colorAction = menu.addAction("Change color");
    QAction *colorAllAction = menu.addAction("Change color all");

    QAction *selectedAction = menu.exec(event->screenPos());

    if (selectedAction)
    {
        if ( selectedAction->text() == removeAction->text())
            emit removeScene(event->scenePos());

        else if (selectedAction->text() == colorAllAction->text())
        {
            QColor color = QColorDialog::getColor(Qt::green);
            emit changeAllColor(color);
        }
        else if (selectedAction->text() == colorAction->text())
        {
            QColor color = QColorDialog::getColor(Qt::green);
            emit changeColor(color, event->scenePos());
        }
        else if (selectedAction->text() == labelObjectAction->text())
            emit addLabel(event->scenePos());

        else if (selectedAction->text() == NameObjectAction->text())
            emit addName(event->scenePos());

        else if (selectedAction->text() == modifyAction->text())
            emit modifyObject(event->scenePos());
    }

}


void RectangleItem::setName(QString name)
{
    strName = name;
}

QString RectangleItem::getName()
{
    return strName;
}

void RectangleItem::setLabel(QString label)
{
    strLabel = label;
}

QString RectangleItem::getLabel()
{
    return strLabel;
}

void RectangleItem::setX(int valueX)
{
    iValueX = valueX;
}

int RectangleItem::getX()
{
    return iValueX;
}

void RectangleItem::setY(int valueY)
{
    iValueY = valueY;
}

int RectangleItem::getY()
{
    return iValueY;
}

void RectangleItem::setWidth(unsigned int width)
{
    uiWidth = width;
}

unsigned int RectangleItem::getWidth()
{
    return uiWidth;
}

void RectangleItem::setHeigth(unsigned int heigth)
{
    uiHeigth = heigth;
}

unsigned int RectangleItem::getHeigth()
{
    return uiHeigth;
}

void RectangleItem::setColor(sColor color)
{
    this->color = color;
}

sColor RectangleItem::getColor()
{
    return color;
}
