#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "player.h"
#include "shared/shared.h"
#include <memory>

class QGraphicsPixmapItem;
class ShapeScene;
class TrackController;
class InitializerSelector;


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    explicit MainWindow(QWidget *parent = 0);
    PlayerWidget& getPlayer();
    QString getPathVideo();
    void setPathVideo(QString);
    void msleep(int ms);
    int getResizedValue();
    void resizeEvent(QResizeEvent *e) override;
    void startSelectorProcess();
    void showSelectorDialog();

    ~MainWindow();

private:

    void initFrameUI();
    void setUpConnections();
    void setUpSelector();
    void setUpResizeVideo();
    void updateImageOfScene(int w, int h);
    void updateNumberFrameuI();
    bool isTrackingActivated();
    void setTrackingActivated(bool value);
    void disableTracking();
    void enableTracking();
    void updateSliderUI(int);
    void refreshScene();


private slots:

    void on_actionOpen_triggered();

    void on_actionPause_triggered();

    void on_actionPlay_triggered();

    void on_actionStop_triggered();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_horizontalSlider_sliderPressed();

    void on_horizontalSlider_sliderReleased();

    void updateObjectUI(const QImage&);

    void on_horizontalSlider_sliderMoved(int position);

    void renderObjectPositions(const std::vector<objectDetails>&, const long long int &numberFrame = 0, bool bUpdateImage = false);

    void display(const std::vector<objectDetails> &objectsDetected, const long long int&);

    void UpdateToFirstFrame();

    void setName(QString name);

    void enableTrackingButton(bool);

    void on_actionConfigure_path_to_save_files_triggered();


    void on_actionConfigure_detection_triggered();

signals:

    void initializePositions(int numFrame);
    void stop();


private:

    Ui::MainWindow *ui;
    PlayerWidget player;
    QString fileName;
    QString pathVideo;
    std::shared_ptr<QGraphicsPixmapItem> imageItem;
    std::shared_ptr<ShapeScene> currentScene;
    TrackController *controller;
    InitializerSelector* selectorDialog;
    bool isVideoLoaded;
    bool trackingActivated;
    bool bUpdateFirstFrame;
    const int INITIAL_FRAME = 0;
    int widthImage;
    int heigthImage;
};

#endif // MAINWINDOW_H
