#-------------------------------------------------
#
# Project created by QtCreator 2017-07-13T10:13:50
#
#-------------------------------------------------

QT       += core gui concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Detector
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    tracker/threadsmanager.cpp \
    rectangleitem.cpp \
    trackercontroller.cpp \
    shapescene.cpp \
    initializer/HistogramColorDetector.cpp \
    initializer/kMeansColorDetector.cpp \
    selector/initializerselector.cpp \
    initializer/HistogramColorDetector.cpp \
    initializer/kMeansColorDetector.cpp \
    tracker/threadsmanager.cpp \
    main.cpp \
    mainwindow.cpp \
    rectangleitem.cpp \
    shapescene.cpp \
    trackercontroller.cpp \
    initializer/processimagedetector.cpp \
    initializer/ColorHandler.cpp \
    initializer/objectdetection.cpp \
    initializer/objectDetectionByColorMethod.cpp \
    initializer/objectsinitializer.cpp \
    tracker/objecttracker.cpp \
    player.cpp \
    initializer/dominantcolordetector.cpp


HEADERS += \
        mainwindow.h \
    shared/shared.h \
    tracker/threadsmanager.h \
    rectangleitem.h \
    trackercontroller.h \
    shapescene.h \
    initializer/HistogramColorDetector.h \
    initializer/kMeansColorDetector.h \
    selector/initializerselector.h \
    initializer/HistogramColorDetector.h \
    initializer/kMeansColorDetector.h \
    shared/shared.h \
    tracker/threadsmanager.h \
    mainwindow.h \
    rectangleitem.h \
    shapescene.h \
    trackercontroller.h \
    initializer/processimagedetector.h \
    initializer/ColorDetector.h \
    initializer/ColorHandler.h \
    initializer/objectdetection.h \
    initializer/ObjectDetectionByColorMethod.h \
    initializer/objectsinitializer.h \
    tracker/objectracker.h \
    player.h \
    initializer/dominantcolordetector.h

INCLUDEPATH += \
    C:/work/soccerAnalyzerDemo/v3/Source/Dependencies/tracker/header \
    C:/work/soccerAnalyzerDemo/v3/Source/Dependencies/bgslibrary/include \
    C:/work/soccerAnalyzerDemo/v3/Source/Dependencies/opencv3.2/include \
    C:/work/soccerAnalyzerDemo/v3/Source/Dependencies/opencv3.2/include/opencv2/core/include \
    C:/work/soccerAnalyzerDemo/v3/Source/Dependencies/opencv3.2/include/opencv2/flann/include \
    C:/work/soccerAnalyzerDemo/v3/Source/Dependencies/opencv3.2/include/opencv2/imgproc/include \
    C:/work/soccerAnalyzerDemo/v3/Source/Dependencies/opencv3.2/include/opencv2/ml/include \
    C:/work/soccerAnalyzerDemo/v3/Source/Dependencies/opencv3.2/include/opencv2/video/include \
    C:/work/soccerAnalyzerDemo/v3/Source/Dependencies/opencv3.2/include/opencv2/imgcodecs/include \
    C:/work/soccerAnalyzerDemo/v3/Source/Dependencies/opencv3.2/include/opencv2/photo/include \
    C:/work/soccerAnalyzerDemo/v3/Source/Dependencies/opencv3.2/include/opencv2/shape/include \
    C:/work/soccerAnalyzerDemo/v3/Source/Dependencies/opencv3.2/include/opencv2/videoio/include \
    C:/work/soccerAnalyzerDemo/v3/Source/Dependencies/opencv3.2/include/opencv2/highgui/include \
    C:/work/soccerAnalyzerDemo/v3/Source/Dependencies/opencv3.2/include/opencv2/objdetect/include \
    C:/work/soccerAnalyzerDemo/v3/Source/Dependencies/opencv3.2/include/opencv2/ts/include \
    C:/work/soccerAnalyzerDemo/v3/Source/Dependencies/opencv3.2/include/opencv2/features2d/include \
    C:/work/soccerAnalyzerDemo/v3/Source/Dependencies/opencv3.2/include/opencv2/calib3d/include \
    C:/work/soccerAnalyzerDemo/v3/Source/Dependencies/opencv3.2/include/opencv2/stitching/include \
    C:/work/soccerAnalyzerDemo/v3/Source/Dependencies/opencv3.2/include/opencv2/superres/include \
    C:/work/soccerAnalyzerDemo/v3/Source/Dependencies/opencv3.2/include/opencv2/videostab/include \

LIBS += -L"C:/work/soccerAnalyzerDemo/v3/Source/Dependencies/tracker/lib/Debug" -lTracker
LIBS += -L"C:/work/soccerAnalyzerDemo/v3/Source/Dependencies/bgslibrary/libs/Debug" -llibbgsd

LIBS += -L"C:/work/soccerAnalyzerDemo/v3/Source/Dependencies/opencv3.2/lib/vc14/Debug" \
    -lopencv_shape320d \
    -lopencv_stitching320d \
    -lopencv_superres320d \
    -lopencv_videostab320d \
    -lopencv_calib3d320d \
    -lopencv_features2d320d \
    -lopencv_flann320d \
    -lopencv_objdetect320d \
    -lopencv_highgui320d \
    -lopencv_ml320d \
    -lopencv_photo320d \
    -lopencv_video320d \
    -lopencv_videoio320d \
    -lopencv_imgcodecs320d \
    -lopencv_imgproc320d \
    -lopencv_core320d \

FORMS += \
        mainwindow.ui

RESOURCES += \
    resources.qrc
