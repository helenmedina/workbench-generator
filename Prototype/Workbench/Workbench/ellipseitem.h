#ifndef ELLIPSEITEM_H
#define ELLIPSEITEM_H

#include <QGraphicsEllipseItem>

class EllipseItem : public QObject, public QGraphicsEllipseItem
{

Q_OBJECT

public:

    EllipseItem(float x, float y, float w, float h);
    void setText(QString);
    QString getText();

private:
    QString ellipseLabel;

protected:

    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);

signals:
    void removeMarkerFromScene(const QPointF & point);

};

#endif // ELLIPSEITEM_H
