#include <QFileDialog>
#include <QMessageBox>
#include <QGraphicsPixmapItem>
#include <QTime>
#include <iostream>
#include <QLineEdit>
#include <QInputDialog>
#include "tracker/threadsmanager.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "trackercontroller.h"
#include "shapescene.h"
#include "rectangleitem.h"
#include "selector/initializerselector.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    initFrameUI();

    isVideoLoaded = false;
    trackingActivated = false;
    bUpdateFirstFrame = false,
    widthImage = 0;
    heigthImage = 0;

    setUpResizeVideo();
    setUpSelector();
    setUpConnections();

    this->setWindowTitle( "Detector Configuration" );
    centralWidget()->setLayout(ui->mainLayout);
}

void MainWindow::setUpResizeVideo()
{
    controller = TrackController::getInstance();
}

void MainWindow::setUpSelector()
{
    selectorDialog = new InitializerSelector(this);
    selectorDialog->setHidden(true);
    ui->actionConfigure_detection->setDisabled(false);
    ui->actionConfigure_path_to_save_files->setDisabled(true);
}

void MainWindow::initFrameUI()
{
    ui->frame_video->setObjectName("frame_video");
    ui->frame_video->setStyleSheet("#frame_video { border: 2px solid white; }");
    ui->frame_process->setObjectName("frame_process");
    ui->frame_process->setStyleSheet("#frame_process { border: 2px solid white; }");

    ui->pushButton->setDisabled(true);
    ui->pushButton->setToolTip("Detect and initialize objects position int the playfield.");
    ui->pushButton_2->setDisabled(true);
    ui->pushButton_2->setToolTip("Starts tracking system to extract objects position during the game.");

}

void MainWindow::setUpConnections()
{
    //Add the connection between controller and view
    QObject::connect(&player, SIGNAL(processImage(const QImage&)), this, SLOT(updateObjectUI(const QImage&)), Qt::DirectConnection);
    QObject::connect(controller, SIGNAL(renderObjectPositions(const std::vector<objectDetails>&, const long long int &, bool)), this, SLOT(renderObjectPositions(const std::vector<objectDetails>&, const long long int &, bool)), Qt::DirectConnection);
    QObject::connect(controller, SIGNAL(displayWindow(const std::vector<objectDetails>&, const long long int&)), this, SLOT(display(const std::vector<objectDetails>&, const long long int&)), Qt::DirectConnection);
    QObject::connect(controller, SIGNAL(stopUpdatingImageUI()), this, SLOT(UpdateToFirstFrame()), Qt::DirectConnection);
    QObject::connect(this, SIGNAL(stop()), controller, SLOT(stop()), Qt::DirectConnection);
    QObject::connect(controller, SIGNAL(enableTrackingButton(bool)), this, SLOT(enableTrackingButton(bool)), Qt::DirectConnection);
    QObject::connect(controller, SIGNAL(clearMarkersUI()), this, SLOT(clearMarkersScene()), Qt::DirectConnection);
    QObject::connect(this, SIGNAL(removeMarker(PointsField)), selectorDialog, SLOT(removeMarker(PointsField)));
    QObject::connect(this, SIGNAL(closeSelectorDialog()), selectorDialog, SLOT(close()));
    QObject::connect(this, SIGNAL(resetSelections()), selectorDialog, SLOT(resetSelectionMarkers()));

}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    //Resize the window if window is bigger or smaller
    if (currentScene.get() != nullptr)
    {
        QTransform transform = ui->graphicsView->transform();
        transform.scale(1.01, 1.01);
        ui->graphicsView->setTransform(transform);
        ui->graphicsView->fitInView(currentScene->sceneRect() , Qt::KeepAspectRatioByExpanding);

    }

}

void MainWindow::updateImageOfScene(int width, int height)
{
    //Shows the new image and setups the objects position for the new image in order to track the object
    widthImage = width;
    heigthImage = height;
    currentScene.reset(new ShapeScene(this));

    QObject::connect(currentScene.get(), SIGNAL(emitName(QString)), this, SLOT(setName(QString)));

    //Shows the image in the scene
    currentScene->setSceneRect(0,0, width, height);
    currentScene->addItem(imageItem.get());
    currentScene->setMode(ShapeScene::Mode::DrawRectangle);
    QRectF rect = currentScene->sceneRect();
    ui->graphicsView->setScene(currentScene.get());
    ui->graphicsView->fitInView(currentScene->sceneRect() , Qt::KeepAspectRatioByExpanding);
    ui->graphicsView->setRenderHints(QPainter::Antialiasing);
    if (!isTrackingActivated())
        refreshScene();

}

void MainWindow::refreshScene()
{
    msleep(player.getDelay());
}

void MainWindow::updateNumberFrameuI()
{
    QString text = ui->num_frame_label->text();
    std::string namelabel = text.toStdString().substr(0, text.toStdString().find(":") +1);
    ui->num_frame_label->setText(QString::fromStdString(namelabel) + " " + QString::number(player.getCurrentFrame()));
}

bool MainWindow::isTrackingActivated()
{
    return trackingActivated;
}

void MainWindow::setTrackingActivated(bool value)
{
    trackingActivated = value;
}

void MainWindow::disableTracking()
{
    ui->pushButton_2->setEnabled(true);
    ui->actionPlay->setEnabled(true);

    if (isTrackingActivated())
    {
        setTrackingActivated(false);       
        emit stop();
    }
}

void MainWindow::enableTracking()
{
    setTrackingActivated(true);
    bUpdateFirstFrame = false;
}

QString MainWindow::getPathVideo()
{
    return pathVideo;
}

void MainWindow::setPathVideo(QString path)
{
    pathVideo = path;
}

PlayerWidget& MainWindow::getPlayer()
{
   return player;
}

void MainWindow::msleep(int ms)
{
    QTime dieTime = QTime::currentTime().addMSecs( ms );
    while( QTime::currentTime() < dieTime )
    {
        QCoreApplication::processEvents( QEventLoop::AllEvents, 100 );
    }
}

void MainWindow::updateSliderUI(int num_frames)
{
    ui->horizontalSlider->setRange(0, num_frames);
    ui->horizontalSlider->setValue(player.getCurrentFrame());
}

void MainWindow::startSelectorProcess()
{
    showSelectorDialog();
}

void MainWindow::showSelectorDialog()
{
    selectorDialog->setVisible(true);
    selectorDialog->show();
}

MainWindow::~MainWindow()
{
    controller -> destroy();
    selectorDialog->close();
    delete ui;
}

/************************Signals and slots *************************/

//This function is called when user cliskc open folder button
void MainWindow::on_actionOpen_triggered()
{
    disableTracking();
    QString path = QFileDialog::getOpenFileName(this, "Open a file", "", "Video file (*.*)");
    fileName = QFileInfo(path).fileName();

   if (!path.isEmpty())
   {
       if (!player.loadVideo(path))
       {
           QMessageBox msgBox;
           msgBox.setText("The selected video could not be opened!");
           msgBox.exec();
       }
       else
       {

           int num_frames = player.getNumberOfFrames();

           //set value of total frames
           QString textTotalFrames = ui->num_total_frames_label->text();
           std::string nameLabelTotalFrames = textTotalFrames.toStdString().substr(0, textTotalFrames.toStdString().find(":") +1);
           ui->num_total_frames_label->setText(QString::fromStdString(nameLabelTotalFrames) + " " + QString::number(num_frames));

           //set file name
           QString textVideoName = ui->video_name_label->text();
           std::string nameVideoLabel = textVideoName.toStdString().substr(0, textVideoName.toStdString().find(":") +1);
           ui->video_name_label->setText(QString::fromStdString(nameVideoLabel) + " " + fileName);

           updateSliderUI(num_frames);

           //video params
           setPathVideo(path);
           isVideoLoaded = true;

       }
   }

}


void MainWindow::updateObjectUI(const QImage& image)
{
    imageItem.reset(new QGraphicsPixmapItem(QPixmap::fromImage(image)));
    imageItem->setSelected(false);

    //update image in view
    int widthImg =  image.size().width();
    int heightImg = image.size().height();

    updateImageOfScene(widthImg, heightImg);

    //update slider
    ui->horizontalSlider->setValue(player.getCurrentFrame());

    //update num frame label
    updateNumberFrameuI();

}

//pause button
void MainWindow::on_actionPause_triggered()
{

    player.stop();
    disableTracking();
}

//Play button
void MainWindow::on_actionPlay_triggered()
{
    disableTracking();
    player.play();

}

//Stop button
void MainWindow::on_actionStop_triggered()
{
    bUpdateFirstFrame = true;
    disableTracking();
}

//Initialize
void MainWindow::on_pushButton_clicked()
{
    setCursor(Qt::BusyCursor);

    disableTracking();

    //remove all the objects detected in order to initalize the positions again
    currentScene->removeAllItems();

    if (isVideoLoaded)
    {
        // If there is any error loading the video, reset the current frame to the first frame
        long long int currentFrame = player.getCurrentFrame();
        if (currentFrame < INITIAL_FRAME || currentFrame > player.getNumberOfFrames())
        {
            currentFrame = INITIAL_FRAME;
        }

       //Tracking button disabled
        //Call the initializer to detect objects in image
       controller->getObjectPositions(player.getFrame(), currentFrame);
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("A video has not been found. Please, load a video.");
        msgBox.exec();
    }
}

//This functions is called when user clicks "track objects" button
void MainWindow::on_pushButton_2_clicked()
{
    enableTracking();
    std::vector<RectangleItem*> items = currentScene->getSelectedItems();
    std::vector<objectDetails> vecObjects;
    long long int currentFrame = player.getCurrentFrame();
    //prepare the vector with objects detected to send them to the controller
    //so the thread manager can create a thread for every object detected

    for (int i = 0; i < items.size(); i++)
    {
        RectangleItem* item = items[i];

        objectDetails object;
        object.x = item->getX();
        object.y = item->getY();
        object.w = item->getWidth();
        object.h = item->getHeigth();

        object.objectName = item->getName().toStdString();
        object.objectLabel = item->getLabel().toStdString();
        object.objectColor = item->getColor();
        object.numberFrame = currentFrame;

        vecObjects.push_back(object);

    }

    ui->pushButton_2->setDisabled(true);
    ui->actionPlay->setDisabled(true);
    //Calls controller with objects detected
    controller->startTracking(pathVideo, currentFrame - 1, vecObjects);

}

void MainWindow::on_horizontalSlider_sliderPressed()
{
    on_actionPause_triggered();
}

void MainWindow::on_horizontalSlider_sliderReleased()
{
    player.updateImage();
}

void MainWindow::on_horizontalSlider_sliderMoved(int position)
{
    player.setCurrentFrame(position);
    updateNumberFrameuI();
}

void MainWindow::on_actionConfigure_path_to_save_files_triggered()
{
    QString path = QFileDialog::getExistingDirectory(this, tr("Open Directory"),"",
                                                     QFileDialog::ShowDirsOnly);
    if (!path.isEmpty())
    {
        controller->setFileNamePath(path + "/");
    }

}

void MainWindow::renderObjectPositions(const std::vector<objectDetails>& objects, const long long int &numberFrame , bool bUpdateImage)
{

    if (bUpdateImage)
    {
        player.setCurrentFrame(numberFrame);
        player.updateImage();
    }
    for(unsigned int i = 0; i < objects.size(); i++)
    {
        objectDetails object = objects[i];
        currentScene->addNewItemFromController(object);
    }

}


void MainWindow::display(const std::vector<objectDetails> &objectsDetected, const long long int &currentFrame)
{
    player.setCurrentFrame(currentFrame);
    player.updateImage();
    if (!objectsDetected.empty())
      renderObjectPositions(objectsDetected);
    msleep(1);
}

void MainWindow::UpdateToFirstFrame()
{
    if (bUpdateFirstFrame)
    {
        player.stop();
        player.setCurrentFrame(INITIAL_FRAME);
        player.updateImage();
    }
}

void MainWindow::setName(QString name)
{
    QString text = ui->object_name_label->text();
    std::string namelabel = text.toStdString().substr(0, text.toStdString().find(":") +1);
    ui->object_name_label->setText(QString::fromStdString(namelabel) + " " + name);
}

void MainWindow::enableTrackingButton(bool bIsEnable)
{
    ui->pushButton_2->setEnabled(bIsEnable);

    if (!bIsEnable)
    {
        setCursor(Qt::ArrowCursor);
        msleep(1);
    }

}


void MainWindow::on_actionConfigure_detection_triggered()
{
    startSelectorProcess();

}
