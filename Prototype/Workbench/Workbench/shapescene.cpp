#include "shapescene.h"
#include "rectangleitem.h"
#include <iostream>
#include <QLineEdit>
#include <QInputDialog>
#include <QDir>
#include <QMessageBox>
#include <QVBoxLayout>
#include <random>

ShapeScene::ShapeScene(QObject* parent): QGraphicsScene(parent)
{
    sceneMode = NoMode;
    pressed = false;
    pLineEdit = new QLineEdit("");
    pLineEdit->setVisible(false);
    pLineEdit->setDisabled(true);
    currentRectangle = -1;
    bIsNewItem = true;
    isItemModified = false;
    positionX = 0;
    positionY = 0;
    reserveMaxRectItems(MAXIMUM_PLAYERS);
}

void ShapeScene::setMode(Mode mode)
{
    sceneMode = mode;
    //set up the item QGraphicsItem features to be controlled by the scene
    makeItemsControllable(false);

}

void ShapeScene::reserveMaxRectItems(int maxValue)
{
    rectangle = new RectangleItem*[maxValue];
    for (int i = 0; i < maxValue; i++)
    {
        rectangle[i] = new RectangleItem();
        rectangle[i] = 0;
    }

}


void ShapeScene::setUpConnectionsPerRectItem(RectangleItem* rectItem)
{
    connect(rectItem, SIGNAL(removeScene(const QPointF &)),this, SLOT(removeMyItem(const QPointF &)));
    connect(rectItem, SIGNAL(changeColor(QColor, const QPointF &)),this, SLOT(changeColor(QColor, const QPointF &)));
    connect(rectItem, SIGNAL(changeAllColor(QColor)),this, SLOT(changeAllColor(QColor)));
    connect(rectItem, SIGNAL(addLabel(const QPointF &)),this, SLOT(addLabel(const QPointF &)));
    connect(rectItem, SIGNAL(addName(const QPointF &)),this, SLOT(addName(const QPointF &)));
    connect(rectItem, SIGNAL(modifyObject(const QPointF &)),this, SLOT(modifyObject(const QPointF &)));

}

void ShapeScene::makeItemsControllable(bool areControllable)
{
    foreach(QGraphicsItem* item, items())
    {
        item->setFlag(QGraphicsItem::ItemIsSelectable, areControllable);
        item->setFlag(QGraphicsItem::ItemIsMovable, areControllable);
    }
}

const std::vector<RectangleItem*>& ShapeScene::getSelectedItems()
{
    return rectangleUI;
}

void ShapeScene::addNewRectangleItem(RectangleItem* newItem)
{
    if (newItem->getLabel().isEmpty())
    rectangleUI.push_back(newItem);
}


void ShapeScene::removeRectItem(RectangleItem* rectItem, int pos)
{

    QString name = rectItem->getName();
    int x = rectItem->getX();
    int y = rectItem->getY();
    unsigned int w = rectItem->getWidth();
    unsigned int h = rectItem->getHeigth();
    if (x == rectangleUI[pos]->getX() && y == rectangleUI[pos]->getY() && name == rectangleUI[pos]->getName()
            && w == rectangleUI[pos]->getWidth() && h == rectangleUI[pos]->getHeigth())
    {
        currentRectangle--;
        rectangleUI.erase (rectangleUI.begin()+pos);
    }

}

RectangleItem* ShapeScene::getNextRectangleItem()
{
    //if limit is reached, last element is returned
    if(!hasRectangleItemsReachlimit())
    {
        currentRectangle++;
        rectangle[currentRectangle] = new RectangleItem();
        return rectangle[currentRectangle];
    }
    else
        return rectangle[currentRectangle];

}


RectangleItem* ShapeScene::getCurrentRectangleItem()
{
    return rectangle[currentRectangle];
}

void ShapeScene::drawRectangleItem(int x, int y, int width, int height)
{
    RectangleItem* item = getCurrentRectangleItem();

    item->setRect(x, y, width, height);

}

bool ShapeScene::hasRectangleItemsReachlimit()
{
    bool bLimit;
    //currentRectangle starts from zero position
   (currentRectangle >= MAXIMUM_PLAYERS - 1) ? bLimit = true: bLimit = false;

    return bLimit;
}

int ShapeScene::getPositionX()
{
    return positionX;
}

int ShapeScene::getPositionY()
{
    return positionY;
}

void ShapeScene::addNewItemToScene(RectangleItem* newItem)
{
    this->addItem(newItem);
    addNewRectangleItem(newItem);
}

void ShapeScene::setInitParametersForRectangleItem(RectangleItem* item, int x, int y, sColor color)
{
    item->setX(x);
    item->setY(y);
    item->setSelected(true);
    if (isItemModified)
    {
        item->setLabel(QString::fromStdString(itemModified.objectLabel));
        item->setName(QString::fromStdString(itemModified.objectName));
        item->setColor(itemModified.objectColor);
        item->setPen(QPen(QColor(itemModified.objectColor.red, itemModified.objectColor.green, itemModified.objectColor.blue), 1, Qt::SolidLine));
        isItemModified =false;
    }
    else
    {
        item->setColor(color);
        item->setPen(QPen(QColor(color.red, color.green, color.blue), 1, Qt::SolidLine));
    }

    setUpConnectionsPerRectItem(item);
}

void ShapeScene::removeAllItems()
{
    foreach(auto item, this->items())
    {
        if (item->type() == RectangleItem::Type)
        {
            RectangleItem* rectItem = dynamic_cast<RectangleItem*>(item);

            if (rectItem != nullptr)
            {
                removeItem(item);
                //Reset number of item in the scene
                currentRectangle = -1;
                rectangleUI.clear();

            }


        }

    }
}

/********************************************* Signals and slots **************************************************/

void ShapeScene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{

    if (event->button() == Qt::LeftButton)
    {
        if(sceneMode == DrawRectangle)
        {
            origPoint = event->scenePos();
            pressed = true;
        }

    }

    QGraphicsScene::mousePressEvent(event);

}

void ShapeScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    bool bNotDrawIfLimitISReached = false;

    if(sceneMode == DrawRectangle  && pressed)
    {
        if (bIsNewItem)
        {
            if(hasRectangleItemsReachlimit())
            {
                bNotDrawIfLimitISReached = true;
                QMessageBox msgBox;
                msgBox.setText("The limit of selections has been reached.");
                msgBox.exec();
            }
            else
            {
                RectangleItem *newItem = getNextRectangleItem();

                bIsNewItem = false;
                sColor color;
                color.red = 255;
                color.green = 255;
                color.blue = 255;

                setInitParametersForRectangleItem(newItem, origPoint.x(), origPoint.y(), color);

                addNewItemToScene(newItem);
            }

        }
        if (!bNotDrawIfLimitISReached)
            drawRectangleItem(origPoint.x(), origPoint.y(), event->scenePos().x() - origPoint.x(), event->scenePos().y() - origPoint.y());


    }

    else
        QGraphicsScene::mouseMoveEvent(event);


}

void ShapeScene::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{

    foreach(auto item, items(event->scenePos())) {
        if (  item->type() == RectangleItem::Type)
        {
            RectangleItem* itemRed = dynamic_cast<RectangleItem*>(item);
            QString name = itemRed->getName();
            if (name.isEmpty())
                name = "[ " + itemRed->getLabel() + " ]";
            else
                name = "[ " + itemRed->getLabel() + "-" + name + " ]";

            emit emitName(name);

            break;
        }
    }

}

void ShapeScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->button() == Qt::LeftButton && !bIsNewItem)
    {
        pressed = false;
        bIsNewItem = true;

        RectangleItem *currentItem = getCurrentRectangleItem();

        if(currentItem != nullptr)
        {
            getCurrentRectangleItem()->setWidth(unsigned int (event->scenePos().x() - currentItem->getX()));
            getCurrentRectangleItem()->setHeigth(unsigned int (event->scenePos().y() - currentItem->getY()));
        }
    }


    QGraphicsScene::mouseReleaseEvent(event);
}

/********************************************* Actions **************************************************/

void ShapeScene::removeMyItem(const QPointF & point)
{
    foreach(auto item, items(point))
    {
        //if ( item->type() != QGraphicsPixmapItem::Type )
        if ( item->type() == RectangleItem::Type )
        {
            removeMyItem(item);

            break;
        }

    }
}

void ShapeScene::removeMyItem(QGraphicsItem *item)
{
    RectangleItem* itemRectangle = dynamic_cast<RectangleItem*>(item);
    if (itemRectangle != nullptr)
    {
        removeItem(item);

        //remove rectangle item stored in vector shared with the controller
        for (int i = 0; i < rectangleUI.size(); i++)
        {
            removeRectItem(itemRectangle, i);
        }

    }

}

void ShapeScene::changeAllColor(QColor color)
{
    sColor colorItem;
    colorItem.red = color.red();
    colorItem.green = color.green();
    colorItem.blue = color.blue();

    foreach(auto item, this->items())
    {
        if (item->type() == RectangleItem::Type)
        {
            RectangleItem* rectItem = dynamic_cast<RectangleItem*>(item);

            if (rectItem != nullptr)
            {
                rectItem->setPen(QPen(QColor(colorItem.red, colorItem.green, colorItem.blue), 1, Qt::SolidLine));
                rectItem->setColor(colorItem);
            }


        }

    }

}

void ShapeScene::changeColor(QColor color, const QPointF & point)
{

    foreach(auto item, items(point))
    {
        if (item->type() == RectangleItem::Type)
        {
           RectangleItem* rectItem = dynamic_cast<RectangleItem*>(item);

           if (rectItem != nullptr)
           {
               sColor colorItem;
               colorItem.red = color.red();
               colorItem.green = color.green();
               colorItem.blue = color.blue();

               rectItem->setPen(QPen(QColor(colorItem.red, colorItem.green, colorItem.blue), 1, Qt::SolidLine));
               rectItem->setColor(colorItem);
           }


            break;
        }

    }

}

void ShapeScene::addName(const QPointF & point)
{

    foreach(auto item, items(point))
    {
        if ( item->type() == RectangleItem::Type )
        {
            bool ok;
            pLineEdit->setVisible(false);

            QString text = QInputDialog::getText(this->pLineEdit, tr("QInputDialog::getText()"),
                                                 tr("Object name:"), QLineEdit::Normal, 0, &ok);



            RectangleItem* rectItem = dynamic_cast<RectangleItem*>(item);
            if (ok && !text.isEmpty())
                rectItem->setName(text);
            break;

        }

    }
}


void ShapeScene::modifyObject(const QPointF & point)
{
    foreach(auto item, items(point))
    {
        if ( item->type() == RectangleItem::Type )
        {
            RectangleItem* itemRectangle = dynamic_cast<RectangleItem*>(item);

            itemModified.objectLabel = itemRectangle->getLabel().toStdString();
            itemModified.objectName = itemRectangle->getName().toStdString();
            itemModified.objectColor = itemRectangle->getColor();
            removeMyItem(itemRectangle);
            isItemModified = true;
        }
    }

}

void ShapeScene::addLabel(const QPointF & point)
{

    foreach(auto item, items(point))
    {
        if ( item->type() == RectangleItem::Type )
        {
            bool ok;
            pLineEdit->setVisible(false);

            QString text = QInputDialog::getText(this->pLineEdit, tr("QInputDialog::getText()"),
                                                 tr("Object label:"), QLineEdit::Normal, 0, &ok);

             RectangleItem* rectItem = dynamic_cast<RectangleItem*>(item);
            if (ok && !text.isEmpty())
                rectItem->setLabel(text);
            break;

        }

    }
}


void ShapeScene::addNewItemFromController(objectDetails object)
{
    RectangleItem *newItem = getNextRectangleItem();
    int x = object.x;
    int y = object.y;
    unsigned int width = object.w;
    unsigned int height = object.h;
    sColor colorObject = object.objectColor;

    setInitParametersForRectangleItem(newItem, x, y, colorObject);
    newItem->setWidth(width);
    newItem->setHeigth(height);
    newItem->setName(QString::fromStdString(object.objectName));
    newItem->setLabel(QString::fromStdString(object.objectLabel));
    addNewItemToScene(newItem);
    drawRectangleItem(x, y, width, height);
}


ShapeScene::~ShapeScene()
{
    //Release memory for rectangle drawing
    for (int i = 0; i < MAXIMUM_PLAYERS; i++)
    {
        if (rectangle[i] != nullptr)
            delete rectangle[i];
    }

    delete []rectangle;

}
