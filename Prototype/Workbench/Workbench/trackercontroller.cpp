#include "trackerController.h"
#include "mainwindow.h"
#include "tracker/threadsmanager.h"
#include <vector>
#include <memory>
#include <QMessageBox>
#include <QErrorMessage>
#include <tracker/objectracker.h>


TrackController* TrackController::singletonController;

TrackController::TrackController()
{}

TrackController* TrackController::getInstance()
{
    if (singletonController == nullptr)
        singletonController = new TrackController;

    return singletonController;
}

void TrackController::destroy()
{
    if (singletonController != nullptr)
    {
        delete singletonController;
        singletonController= 0;
    }
}
//This function detects objects in image
void TrackController::getObjectPositions(cv::Mat &frame, long long int numberFrame)
{
    //This function could be modified with the algorithms selected for the user
    //In this prototype this parts is not automatized yet
    std::vector<objectDetails> objectsPosition;
    bool bObjectDetected = initializer.detectObjects(frame, objectsPosition);

    //Sends the view the positions of the object detected
    if(bObjectDetected)
    {
       emit renderObjectPositions(objectsPosition, numberFrame, true);
    }

}

void TrackController::startTracking(QString path, int numFrame, std::vector<objectDetails> vecObjects)
{

    //Get frame from video to send to the threadManager
    cv::VideoCapture capture(path.toStdString());
    capture.set(CV_CAP_PROP_POS_FRAMES, numFrame);

    numObjects = vecObjects.size();

    //Start tracking only if objects are detected
    if (numObjects > 0)
    {
        //Set up the init parameters for the threads
        ThreadsManager trackManager;

        //Connections with the children threads
        QObject::connect(&trackManager, SIGNAL(readyToDisplay(const std::vector<objectDetails>&, const long long int&)), this, SLOT(display(const std::vector<objectDetails>&, const long long int&)), Qt::DirectConnection);
        QObject::connect(this, SIGNAL(stopTracking(bool)), &trackManager, SLOT(stopTracking(bool)), Qt::DirectConnection);
        QObject::connect(&trackManager, SIGNAL(errorLabelObject(std::string&)), this, SLOT(showErrorMessage(std::string&)), Qt::DirectConnection);


        trackManager.stopTracking(false);
        trackManager.setCapture(capture);
        trackManager.setFileNamePath(pathToSaveFiles.toStdString());
        if (trackManager.createObjects(numObjects, vecObjects))
        {
            //Start threads
            trackManager.run();
            trackManager.wait();
        }

        enableTrackingButton(true);
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("No objects detected.");
        msgBox.exec();
    }


}

void TrackController::display(const std::vector<objectDetails> &objectsDetected, const long long int &numberFrame)
{
    emit displayWindow(objectsDetected, numberFrame);
}

void TrackController::stop()
{
    emit stopTracking(true);
}

void TrackController::setFileNamePath(QString path)
{
    this->pathToSaveFiles = path;
}

void TrackController::enableTracking(bool value)
{
    emit enableTrackingButton(value);
}

void TrackController::showErrorMessage(std::string &text)
{
    QErrorMessage messageError;
    messageError.setEnabled(true);
    messageError.showMessage(QString::fromStdString(text));
    messageError.exec();
}

