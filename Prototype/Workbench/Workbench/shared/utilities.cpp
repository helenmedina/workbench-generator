#include "shared/utilities.h"
#include <istream>

void resizeImage(const cv::Mat &imageOriginal, cv::Mat &imageResized, int iResizeValue)
{
    cv::resize(imageOriginal, imageResized, cv::Size(imageOriginal.cols / iResizeValue, imageOriginal.rows / iResizeValue), cv::INTER_NEAREST);
}

void checkColors(sColor &color)
{
    if (color.red > MAX_COLOR_RGB_VALUE || color.red < MIN_COLOR_RGB_VALUE)
        color.red = MAX_COLOR_RGB_VALUE;

    if (color.green > MAX_COLOR_RGB_VALUE || color.green < MIN_COLOR_RGB_VALUE)
        color.green = MAX_COLOR_RGB_VALUE;

    if (color.blue > MAX_COLOR_RGB_VALUE || color.blue < MIN_COLOR_RGB_VALUE)
        color.blue = MAX_COLOR_RGB_VALUE;

}

bool fileExists (const std::string& filename)
{
    std::ifstream f(filename.c_str());
    return f.good();
}

void correctLimitPositions(double &currentValue, const double &bellowLimit, const double &aboveLimit)
{
    if (currentValue < bellowLimit)
        currentValue = bellowLimit;
    if (currentValue > aboveLimit)
        currentValue = aboveLimit;
}

void correctLimitPositions(int &currentValue, const int &bellowLimit, const int &aboveLimit)
{
    if (currentValue < bellowLimit)
        currentValue = bellowLimit;
    if (currentValue > aboveLimit)
        currentValue = aboveLimit;
}

std::string getPlayersLabel(PlayersLabel value)
{
    std::string label;
    switch(value)
    {
        case Label1A:
            label = "1A";
        break;
        case Label2A:
            label = "2A";
        break;
        case Label3A:
            label = "3A";
        break;
        case Label4A:
            label = "4A";
        break;
        case Label5A:
            label = "5A";
        break;
        case Label6A:
            label = "6A";
        break;
        case Label7A:
            label = "7A";
        break;
        case Label8A:
            label = "8A";
        break;
        case Label9A:
            label = "9A";
        break;
        case Label10A:
            label = "10A";
        break;
        case Label11A:
            label = "11A";
        break;
        case Label12A:
            label = "12A";
        break;
        case Label13A:
            label = "13A";
        break;
        case Label14A:
            label = "14A";
        break;
        case Label15A:
            label = "15A";
        break;
        case Label16A:
            label = "16A";
        break;
        case Label17A:
            label = "17A";
        break;
        case Label18A:
            label = "18A";
        break;
        case Label19A:
            label = "19A";
        break;
        case Label20A:
            label = "20A";
        break;
        case Label21A:
            label = "21A";
        break;
        case Label22A:
            label = "22A";
        break;
        case Label23A:
            label = "23A";
        break;
        case Label24A:
            label = "24A";
        break;
        case Label25A:
            label = "25A";
        break;
        case Label1B:
            label = "1B";
        break;
        case Label2B:
            label = "2B";
        break;
        case Label3B:
            label = "3B";
        break;
        case Label4B:
            label = "4B";
        break;
        case Label5B:
            label = "5B";
        break;
        case Label6B:
            label = "6B";
        break;
        case Label7B:
            label = "7B";
        break;
        case Label8B:
            label = "8B";
        break;
        case Label9B:
            label = "9B";
        break;
        case Label10B:
            label = "10B";
        break;
        case Label11B:
            label = "11B";
        break;
        case Label12B:
            label = "12B";
        break;
        case Label13B:
            label = "13B";
        break;
        case Label14B:
            label = "14B";
        break;
        case Label15B:
            label = "15B";
        break;
        case Label16B:
            label = "16B";
        break;
        case Label17B:
            label = "17B";
        break;
        case Label18B:
            label = "18B";
        break;
        case Label19B:
            label = "19B";
        break;
        case Label20B:
            label = "20B";
        break;
        case Label21B:
            label = "21B";
        break;
        case Label22B:
            label = "22B";
        break;
        case Label23B:
            label = "23B";
        break;
        case Label24B:
            label = "24B";
        break;
        case Label25B:
            label = "25B";
        break;
        default:
            label = "25A";

    }

    return label;
}
