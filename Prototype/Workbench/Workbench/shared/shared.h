#ifndef SHARED_H
#define SHARED_H
#include <string>
#include <QPoint>

struct sColor
{
    unsigned int red;
    unsigned green;
    unsigned blue;
};

struct objectDetails
{
    double x;
    double y;
    int w;
    int h;
    std::string objectName;
    std::string objectLabel;
    sColor objectColor;
    long long int numberFrame;
};


enum ObjectsLabel
{
    Label1A = 1,
    Label2A = 2,
    Label3A = 3,
    Label4A = 4,
    Label5A = 5,
    Label6A = 6,
    Label7A = 7,
    Label8A = 8,
    Label9A = 9,
    Label10A = 10,
    Label11A = 11,
    Label12A = 12,
    Label13A = 13,
    Label14A = 14,
    Label15A = 15,
    Label16A = 16,
    Label17A = 17,
    Label18A = 18,
    Label19A = 19,
    Label20A = 20,
    Label21A = 21,
    Label22A = 22,
    Label23A = 23,
    Label24A = 24,
    Label25A = 25,
    Label1B = 26,
    Label2B = 27,
    Label3B = 28,
    Label4B = 29,
    Label5B = 30,
    Label6B = 31,
    Label7B = 32,
    Label8B = 33,
    Label9B = 34,
    Label10B = 35,
    Label11B = 36,
    Label12B = 37,
    Label13B = 38,
    Label14B = 39,
    Label15B = 40,
    Label16B = 41,
    Label17B = 42,
    Label18B = 43,
    Label19B = 44,
    Label20B = 45,
    Label21B = 46,
    Label22B = 47,
    Label23B = 48,
    Label24B = 49,
    Label25B = 50

};

const int MAX_COLOR_RGB_VALUE = 255;
const int MIN_COLOR_RGB_VALUE = 0;

#endif // SHARED_H
