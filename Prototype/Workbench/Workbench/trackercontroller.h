#ifndef TRACKERCONTROLLER_H
#define TRACKERCONTROLLER_H

#include <QWidget>
#include "initializer/objectsinitializer.h"
#include <tracker/threadsmanager.h>


class MainWindow;
class RectangleItem;

class TrackController: public QWidget
{
    Q_OBJECT

public:
    static TrackController *getInstance();
    static void destroy();
    void getObjectPositions(cv::Mat &frame, long long int);
    void startTracking(QString path, int numFrame, std::vector<objectDetails>);
    void setFileNamePath(QString path);

private:
    TrackController();
    TrackController(const TrackController &);
    TrackController & operator = (const TrackController &);
    static TrackController * singletonController;

private:
    objectsInitializer initializer;
    //ObjectTracker **objectsDetected2;
    int numObjects;
    QString pathToSaveFiles;


public slots:
    void display(const std::vector<objectDetails>&, const long long int&);
    void stop();
    void enableTracking(bool);
    void showErrorMessage(std::string&);


signals:
    void renderObjectPositions(const std::vector<objectDetails>&, const long long int &, bool);
    void displayWindow(const std::vector<objectDetails> &objectsDetected, const long long int &);
    void stopTracking(bool);
    void enableTrackingButton(bool);

};

#endif // TRACKERCONTROLLER_H
