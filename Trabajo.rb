require_relative 'XmlParser.rb'
require 'erb'

#types figure ellipse or rectangle
typeFigure = "Rectangle" 
numberTrackers = "45"

def createInterfaceDetector(detector)

	erb = ERB.new( File.open( "./Templates/InterfaceDetector.h" ).read, nil, '-' )
	@ProgramFile = erb.result( binding )
	name = "./Output/headers/#{detector.getName}Detector.h"
	File.open(name, "w" ).write( @ProgramFile )

	print "Creating #{detector.getName}Detector.h file\n"

end

def createHandlerDetector(detector)

	erb = ERB.new( File.open( "./Templates/HandlerDetector.h" ).read, nil, '-' )
	@ProgramFile = erb.result( binding )
	name = "./Output/headers/#{detector.getName}Handler.h"
	File.open(name, "w" ).write( @ProgramFile )

	print "Creating #{detector.getName}Handler.h file\n"


	erb = ERB.new( File.open( "./Templates/HandlerDetector.cpp" ).read, nil, '-' )
	@ProgramFile = erb.result( binding )
	name = "./Output/source/#{detector.getName}Handler.cpp"
	File.open(name, "w" ).write( @ProgramFile )

	print "Creating #{detector.getName}Handler.cpp file\n"

end

def createSelectorMethod(detector)

	erb = ERB.new( File.open( "./Templates/SelectorMethodDetector.h" ).read, nil, '-' )
	@ProgramFile = erb.result( binding )
	name = "./Output/headers/ObjectDetectionBy#{detector.getName}Method.h"
	File.open(name, "w" ).write( @ProgramFile )

	print "Creating ObjectDetectionBy#{detector.getName}Method.h file\n"

	erb = ERB.new( File.open( "./Templates/SelectorMethodDetector.cpp" ).read, nil, '-' )
	@ProgramFile = erb.result( binding )
	name = "./Output/source/ObjectDetectionBy#{detector.getName}Method.pp"
	File.open(name, "w" ).write( @ProgramFile )

	print "Creating ObjectDetectionBy#{detector.getName}Method.cpp file\n"

end

def createChildren(detector)


	detector.getChildren.each do |child|
	erb = ERB.new( File.open( "./Templates/ChildDetector.h" ).read, nil, '-' )
	@ProgramFile = erb.result( binding )
	name = "./Output/headers/#{child[:name]}Detector.h"
	File.open(name, "w" ).write( @ProgramFile )

	print "Creating #{child[:name]}Detector.h file\n"

	end


	detector.getChildren.each do |child|
	erb = ERB.new( File.open( "./Templates/ChildDetector.cpp" ).read, nil, '-' )
	@ProgramFile = erb.result( binding )
	name = "./Output/source/#{child[:name]}Detector.cpp"
	File.open(name, "w" ).write( @ProgramFile )

	print "Creating #{child[:name]}Detector.cpp file\n"

	end

end

def defineMaxNumberObjects(numberTrackers)

	erb = ERB.new( File.open( "./Templates/shapescene.h" ).read, nil, '-' )
	@ProgramFile = erb.result( binding )
	name = "./Output/headers/shapescene.h"
	File.open(name, "w" ).write( @ProgramFile )

	print "Creating shapescene.h file\n"

end

def createFigureType(figure)

	erb = ERB.new( File.open( "./Templates/figure.h" ).read, nil, '-' )
	@ProgramFile = erb.result( binding )
	name = "./Output/headers/#{figure}Item.h"
	File.open(name, "w" ).write( @ProgramFile )

	print "Creating #{figure}Item.h file\n"

	erb = ERB.new( File.open( "./Templates/figure.cpp" ).read, nil, '-' )
	@ProgramFile = erb.result( binding )
	name = "./Output/source/#{figure}Item.cpp"
	File.open(name, "w" ).write( @ProgramFile )

	print "Creating #{figure}Item.cpp file\n"

end

def createSelectorDetector(detectors, interfaces)

	erb = ERB.new( File.open( "./Templates/initializerselector.h" ).read, nil, '-' )
	@ProgramFile = erb.result( binding )
	name = "./Output/headers/initializerselector.h"
	File.open(name, "w" ).write( @ProgramFile )

	print "Creating initializerselector.h file\n"

	erb = ERB.new( File.open( "./Templates/initializerselector.cpp" ).read, nil, '-' )
	@ProgramFile = erb.result( binding )
	name = "./Output/source/initializerselector.cpp"
	File.open(name, "w" ).write( @ProgramFile )

	print "Creating initializerselector.cpp file\n"

end

begin

	fileName = 'input.xml'
	doc = REXML::Document.new( File.open( "#{fileName}" ) )
	
	#parse the input file
	parser = XmlParser.new
	parser.parse(doc)
	detectors =  parser.getDetectors
	interfaces = parser.getDetectorsInterface
	#creates headers and source files of the prototype
	detectors.each do |detector|

		createInterfaceDetector(detector)
		createHandlerDetector(detector)
		createChildren(detector)		
		createSelectorMethod(detector)

	end
	createSelectorDetector(detectors, interfaces)
	defineMaxNumberObjects(numberTrackers)
	createFigureType(typeFigure)
	
	
rescue REXML::ParseException => e
	puts "Failed when parsing document: #{e.message}"
rescue RuntimeError => e
	puts "Failed: #{e.message}"
rescue 
	puts "Failed: #{$!.message}" 
end

