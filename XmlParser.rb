require 'rexml/document'
require "rexml/parseexception"
require_relative 'Detector.rb'


class XmlParser
	
	def initialize
		@arrayDetectors = []
		@arrayDetectorInterface = []
	end
		
	def parse(doc)
		print "Parsing tables\n"
		doc.root.each_element( "handlerDetector" ) { |tableElement|
			detector_name = tableElement.attributes[ "name" ]
			print "#{detector_name}  \n"
			@detector = Detector.new(detector_name)
			#Get columns information
			setDetectorChildren(@detector, tableElement)
			setVirtualMethod(@detector, tableElement)
			#stores the detectors parsed of the input file
			@arrayDetectors << @detector
			#stores onlye the name of the classes that inherits from interface class
			@arrayDetectorInterface << detector_name

		}

	end

	def setDetectorChildren(detectorObj, element)
		REXML::XPath.match(element, './/detectors/detector').map do |detector|			
			nameDetector = detector.attributes[ "name" ]	
			detectorObj.insertChildren({name: nameDetector})
		end	
	end

	def setVirtualMethod(detectorObj, element)
		
		REXML::XPath.match(element, './/virtual/output').map do |option|			

			if option.text.nil? || option.text.empty?		
				typeReturnMethod = "nil"
					
			else
				typeReturnMethod = option.text.strip	
			end
			
			#replace the characters for "<", ">", as they are not allowed in xml file
			typeReturnMethod = replaceCharacter(typeReturnMethod)

			detectorObj.setOutputMethod(typeReturnMethod)
		end

		REXML::XPath.match(element, './/virtual/name').map do |name|			

			if name.text.nil? || name.text.empty?		
				nameMethod = "nil"
					
			else
				nameMethod = name.text.strip	
			end

			detectorObj.setNameMethod(nameMethod)
		end
		
		setInputsForMethod(detectorObj, element)

	end
	
	def getDetectors()
	
		return @arrayDetectors
	
	end
	def getDetectorsInterface()
	
		return @arrayDetectorInterface
	
	end


	def setInputsForMethod(detectorObj, element)
	
		REXML::XPath.match(element, './/virtual/inputs/input').map do |input|			
			typeInput = input.attributes[ "type" ]
			inputParam = input.text.strip	
			typeInput = replaceCharacter(typeInput)
			detectorObj.insertVirtualInput({name: inputParam, type:typeInput})
		end
	
	end

	def replaceCharacter(typeReturnMethod)
		
		typeReturnMethod.gsub!("00","<")
		typeReturnMethod.gsub!("01",">")
		return typeReturnMethod

	end
	
	
end


