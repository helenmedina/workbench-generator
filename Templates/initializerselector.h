#ifndef INITIALIZERDETECTOR_H
#define INITIALIZERDETECTOR_H
#include <QDialog>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QGroupBox>


class InitializerSelector : public QDialog
{
    Q_OBJECT

public:
    InitializerSelector(QWidget *parent = 0);



private:
<%-detectors.each {|detector|-%>
<%- detector.getChildren.each { |child|-%>
	void addSelectorBy<%= child[:name].capitalize %>(QHBoxLayout *layout);
<%- } -%>
<%- } -%>
    void setUpConfiguration();
 <%-interfaces.each {|detector|-%>
	void setUpSelection<%=detector.capitalize%>(QVBoxLayout *layout);
<%- } -%>
    


private:
<%-interfaces.each {|detector|-%>
    QVBoxLayout *detectBy<%=detector.capitalize%>Group;
    QGroupBox *groupBox<%=detector.capitalize%>;
<%- } -%>
    QPushButton *OKButtom;
    QHBoxLayout *layoutForGroupBoxes;

};
#endif // INITIALIZERDETECTOR_H
