#include "<%= detector.getName %>Handler.h"

void <%= detector.getName.capitalize %>Handler::set<%= detector.getName.capitalize %>Detector(<%= detector.getName.capitalize %>Detector* detector)
{
    this-><%= detector.getName %>Detector = detector;
}

<%=detector.getOutputMethod%> <%= detector.getName.capitalize %>Handler::<%=detector.getNameMethod%>(<%- counter = 0-%> <%-detector.getVirtualInput.each { |input|  if (counter != 0)-%>, <%-else end-%>  <%= input[:type] %>  <%= input[:name] %><%- counter = counter +1}-%>)
{
    paramOutputs = <%= detector.getName %>Detector-><%=detector.getNameMethod%>(image, count);
    return paramOutputs;
}

void <%= detector.getName.capitalize %>Handler::processImage(cv::Mat image)
{
    //process the image with the paramOutputs obtained
}
