#ifndef <%= detector.getName.upcase %>HANDLER_H
#define <%= detector.getName.upcase %>HANDLER_H

#include "<%= detector.getName %>Detector.h"

using namespace std;
using namespace cv;

class <%= detector.getName.capitalize %>Handler
{

public:
    void set<%= detector.getName.capitalize %>Detector(<%= detector.getName.capitalize %>Detector*);
    virtual <%=detector.getOutputMethod%> <%=detector.getNameMethod%>(<%- counter = 0-%> <%-detector.getVirtualInput.each { |input|  if (counter != 0)-%>, <%-else end-%>  <%= input[:type] %>  <%= input[:name] %><%- counter = counter +1}-%>);
    void processImage(Mat image);

private:
    <%=detector.getOutputMethod%> paramOutputs;
    <%= detector.getName.capitalize %>Detector *<%= detector.getName %>Detector;
};

#endif // <%= detector.getName.upcase %>HANDLER_H
