#include "<%= figure %>Item.h"
#include <QMenu>
#include <QGraphicsSceneContextMenuEvent>
#include <QColorDialog>


<%= figure.capitalize %>Item::<%= figure.capitalize %>Item()
{
    iValueX = 0;
    iValueY = 0;
    uiWidth = 0;
    uiHeigth = 0;
}

void <%= figure.capitalize %>Item::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    QMenu menu;

    QAction *modifyAction = menu.addAction("Modify");
    QAction *removeAction = menu.addAction("Remove");
    QAction *labelObjectAction = menu.addAction("Set label object");
    QAction *NameObjectAction = menu.addAction("Set name object");
    QAction *colorAction = menu.addAction("Change color");
    QAction *colorAllAction = menu.addAction("Change color all");

    QAction *selectedAction = menu.exec(event->screenPos());

    if (selectedAction)
    {
        if ( selectedAction->text() == removeAction->text())
            emit removeScene(event->scenePos());

        else if (selectedAction->text() == colorAllAction->text())
        {
            QColor color = QColorDialog::getColor(Qt::green);
            emit changeAllColor(color);
        }
        else if (selectedAction->text() == colorAction->text())
        {
            QColor color = QColorDialog::getColor(Qt::green);
            emit changeColor(color, event->scenePos());
        }
        else if (selectedAction->text() == labelObjectAction->text())
            emit addLabel(event->scenePos());

        else if (selectedAction->text() == NameObjectAction->text())
            emit addName(event->scenePos());

        else if (selectedAction->text() == modifyAction->text())
            emit modifyObject(event->scenePos());
    }

}


void <%= figure.capitalize %>Item::setName(QString name)
{
    strName = name;
}

QString <%= figure.capitalize %>Item::getName()
{
    return strName;
}

void <%= figure.capitalize %>Item::setLabel(QString label)
{
    strLabel = label;
}

QString <%= figure.capitalize %>Item::getLabel()
{
    return strLabel;
}

void <%= figure.capitalize %>Item::setX(int valueX)
{
    iValueX = valueX;
}

int <%= figure.capitalize %>Item::getX()
{
    return iValueX;
}

void <%= figure.capitalize %>Item::setY(int valueY)
{
    iValueY = valueY;
}

int <%= figure.capitalize %>Item::getY()
{
    return iValueY;
}

void <%= figure.capitalize %>Item::setWidth(unsigned int width)
{
    uiWidth = width;
}

unsigned int <%= figure.capitalize %>Item::getWidth()
{
    return uiWidth;
}

void <%= figure.capitalize %>Item::setHeigth(unsigned int heigth)
{
    uiHeigth = heigth;
}

unsigned int <%= figure.capitalize %>Item::getHeigth()
{
    return uiHeigth;
}

void <%= figure.capitalize %>Item::setColor(sColor color)
{
    this->color = color;
}

sColor <%= figure.capitalize %>Item::getColor()
{
    return color;
}
