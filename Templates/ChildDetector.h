#ifndef <%= child[:name].upcase %>DETECTOR_H
#define <%= child[:name].upcase %>DETECTOR_H
#include "<%= detector.getName %>Detector.h"
using namespace std;
using namespace cv;

class <%= child[:name] %>Detector : public <%= detector.getName.capitalize %>Detector
{

public:
    virtual <%=detector.getOutputMethod%> <%=detector.getNameMethod%>(<%- counter = 0-%> <%-detector.getVirtualInput.each { |input|  if (counter != 0)-%>, <%-else end-%>  <%= input[:type] %>  <%= input[:name] %><%- counter = counter +1}-%>);

};
#endif // <%= child[:name].upcase %>DETECTOR_H

