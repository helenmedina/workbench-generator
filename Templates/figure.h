#ifndef <%= figure.upcase %>ITEM_H
#define <%= figure.upcase %>ITEM_H
<%-if figure == "Rectangle"-%>
#include <QGraphicsRectItem>
<%-else-%>
#include <QGraphicsEllipseItem>
<%-end-%>
#include "shared/shared.h"

<%-if figure == "Rectangle"-%>
class <%= figure %>Item : public QObject, public QGraphicsRectItem
<%-else-%>
class <%= figure %>Item : public QObject, public QGraphicsEllipseItem
<%-end-%>
{

Q_OBJECT

public:

    <%= figure.capitalize %>Item();
    void setName(QString);
    QString getName();
    void setLabel(QString name);
    QString getLabel();
    void setX(int);
    int getX();
    void setY(int );
    int getY();
    void setWidth(unsigned int);
    unsigned int getWidth();
    void setHeigth(unsigned int);
    unsigned int getHeigth();
    void setColor(sColor);
    sColor getColor();

private:

    QString strName;
    QString strLabel;
    int iValueX;
    int iValueY;
    unsigned int uiWidth;
    unsigned int uiHeigth;
    sColor color;

protected:

    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);

signals:

    void removeScene(const QPointF & point);
    void changeColor(QColor, const QPointF & point);
    void changeAllColor(QColor);
    void addLabel(const QPointF & point);
    void addName(const QPointF & point);
    void modifyObject(const QPointF & point);
};

#endif // <%= figure.upcase %>ITEM_H
