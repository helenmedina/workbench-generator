#include "<%= child[:name] %>Detector.h"

<%=detector.getOutputMethod%> <%= child[:name] %>Detector::<%=detector.getNameMethod%>(<%- counter = 0-%> <%-detector.getVirtualInput.each { |input|  if (counter != 0)-%>, <%-else end-%>  <%= input[:type] %>  <%= input[:name] %><%- counter = counter +1}-%>)
{
    <%=detector.getOutputMethod%> output;
	//proccess the image load library or add code
    return output;
}
