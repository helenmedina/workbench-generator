
#ifndef <%= detector.getName.upcase %>DETECTOR_H
#define <%= detector.getName.upcase %>DETECTOR_H

#include "shared/shared.h"
#include <vector>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

class <%= detector.getName.capitalize %>Detector
{

public:
	//count -> number of colors to be detected
    virtual <%=detector.getOutputMethod%> <%=detector.getNameMethod%>(<%- counter = 0-%> <%-detector.getVirtualInput.each { |input|  if (counter != 0)-%>, <%-else end-%>  <%= input[:type] %>  <%= input[:name] %><%- counter = counter +1}-%>) = 0;

};


#endif
