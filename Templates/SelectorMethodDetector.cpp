#include "ObjectDetectionBy<%= detector.getName.capitalize %>Method.h"
#include "kmeanscolordetector.h"
#include "colorhandler.h"

void objectDetectionBy<%= detector.getName.capitalize %>Method::processImplementation(cv::Mat &image, cv::Mat &output)
{

    kMeansColorDetector kMeansDetector;
    <%= detector.getName.capitalize %>Handler <%= detector.getName %>Handler;

    <%= detector.getName %>Handler.set<%= detector.getName.capitalize %>Detector(/*add typ detector dinamic*/);

    int input = 1;/*param input define gui*/ 
    <%=detector.getOutputMethod%> candidates = <%= detector.getName %>Handler.<%=detector.getNameMethod%>(image, input);
    <%= detector.getName %>Handler.processImage(candidates, image);
    //filtrra imagen con color detectado
    //fill vector position one it process image una procesado consigue positions de mascara
}

std::vector<objectDetails> objectDetectionBy<%= detector.getName.capitalize %>Method::getObjectDetails()
{
    return this->objectDetails;
}
