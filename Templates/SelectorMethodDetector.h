#ifndef OBJECT_DETECTION_BY_<%= detector.getName.upcase %>_METHOD
#define OBJECT_DETECTION_BY_<%= detector.getName.upcase %>_METHOD

#include "objectdetection.h"
#include "shared/shared.h"

class ObjectDetectionBy<%= detector.getName.capitalize %>Method : public OjectDetection
{
public:
    virtual std::vector<objectDetails> getObjectDetails();

private:
	void processImplementation(cv::Mat &image, cv::Mat &output);



private:
    std::vector<objectDetails> objectDetails;

};

#endif //OBJECT_DETECTION_BY_COLOR_METHOD
