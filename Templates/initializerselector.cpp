#include <selector/initializerselector.h>
#include <QRadioButton>
#include <QGroupBox>
#include <QCheckBox>
#include <QLineEdit>

InitializerSelector::InitializerSelector(QWidget *parent) : QDialog(parent)
{
    layoutForGroupBoxes = new QHBoxLayout;
<%-interfaces.each {|detector|-%>
	detectBy<%=detector.capitalize%>Group = new QVBoxLayout;
<%- } -%>
    
    OKButtom = new QPushButton("Ok");

<%-interfaces.each {|detector|-%>
	setUpSelection<%=detector.capitalize%>(detectBy<%=detector.capitalize%>Group);
<%- } -%>

 
<%-interfaces.each {|detector|-%>
    groupBox<%=detector.capitalize%> = new QGroupBox( "<%=detector.capitalize%>");
    groupBox<%=detector.capitalize%>->setLayout(detectBy<%=detector.capitalize%>Group);
    layoutForGroupBoxes->addWidget(groupBox<%=detector.capitalize%>);
<%- } -%>
  
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addLayout(layoutForGroupBoxes);
    this->setLayout(mainLayout);
}

<%-detectors.each {|detector|-%>

void InitializerSelector::setUpSelection<%=detector.getName.capitalize%>(QVBoxLayout *verticalLayout)
{
    verticalLayout->setSizeConstraint(QLayout::SetFixedSize);

<%- detector.getChildren.each { |child|-%>
	QHBoxLayout *layout<%= child[:name].capitalize %>H = new QHBoxLayout;
    addSelectorBy<%= child[:name].capitalize %>(layout<%= child[:name].capitalize %>H);
<%- } -%>
<%- detector.getChildren.each { |child|-%>
    verticalLayout->addLayout(layout<%= child[:name].capitalize %>H);
<%- } -%>
}
<%- } -%>

<%-detectors.each {|detector|-%>
<%- detector.getChildren.each { |child|-%>
void InitializerSelector::addSelectorBy<%= child[:name].capitalize %>(QHBoxLayout *layout)
{
    QRadioButton *radioSelectorBy<%= child[:name].capitalize %> = new QRadioButton(tr("Detector based on <%= child[:name].capitalize %>"));
    radioSelectorBy<%= child[:name].capitalize %>->setVisible(true);
    QCheckBox *checkbox<%= child[:name].capitalize %>Selector = new QCheckBox(this);
    checkbox<%= child[:name].capitalize %>Selector->setEnabled(false);
    QLineEdit *orderOf<%= child[:name].capitalize %> = new QLineEdit ("1");
    QObject::connect(radioSelectorBy<%= child[:name].capitalize %>, SIGNAL(clicked(bool)), checkbox<%= child[:name].capitalize %>Selector, SLOT(setEnabled(bool)));

    layout->addWidget(radioSelectorBy<%= child[:name].capitalize %>);
    layout->addWidget(checkbox<%= child[:name].capitalize %>Selector);
    layout->addWidget(orderOf<%= child[:name].capitalize %>);
}
<%- } -%>
<%- } -%>
